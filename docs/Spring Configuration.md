# Spring Configuration.

In project we use hybrid Spring configuration via XML **and** JAVA config-files, that is most flexible variant of config.  

#### Table of content
1. [Components](#1.Components)
2. [Description](#2.Description)
2.1. [ AuraConfig.class](#2.1.AuraConfig.class)
2.2. [ AuraConfig.xml](#2.2.AuraConfig.xml)
2.3. [ SecurityConfig.class](#2.3.SecurityConfig.class)


## 1.Components
**AuraConfig.class** is a main configuration point.:
**AuraConfig.xml** contains basic beans configuration for our project.
**SecurityConfig.class** contains configuration for authentication and athorization services provided using Spring Security.


## 2.Description
### 2.1.AuraConfig.class
**AuraConfig.class**  is configured in project's  **web.xml** as contextConfigLocation:
```xml
<context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>academy.softserve.aura.core.config.AuraConfig</param-value>
    </context-param>
```
  All other configuration files (`AuraConfig.xml`, `SecurityConfig.java`) are imported in AuraConfig.java.
  Also we scan all files at `academy.softserve.aura.core` to discover all Spring-annotated classes.
  ```java
  @Configuration
@ImportResource("classpath:/AuraConfig.xml")
@ComponentScan(basePackages = {"academy.softserve.aura.core"})
@Import(SecurityConfig.class)
public class AuraConfig {
}
```

### 2.2.AuraConfig.xml
In **AuraConfig.xml** we turn on Spring MVC:
```xml
<mvc:annotation-driven/>
```
XML - config loads a part of configuration properties from jdbc.properties file, that allow us to have separate configuration for different environments still having consistent Spring config files. **Jdbc.properties** file is loaded by means of Spring Framework within `PropertyPlaceholderConfigurer` bean:
```xml
<bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
    <property name="location" value="classpath:jdbc.properties"/>
</bean>
```
Also in **AuraConfig.xml** we define beans that are necessary to provide support of Hibernate ORM Framework in our project.
We use [**c3p0 library**][1] to configure our  database DataSource as [connection pooled source][2].
>>>
Benefits of Using Connection Pools
Applications that are database-intensive generally benefit the most from connection pools. As a policy, applications should use a connection pool whenever database usage is known to affect application performance.
+ Connection pools provide the following benefits:
+ Reduces the number of times new connection objects are created.
+ Promotes connection object reuse.
+ Quickens the process of getting a connection.
+ Reduces the amount of effort required to manually manage connection objects.
+ Minimizes the number of stale connections.
+ Controls the amount of resources spent on maintaining connections.
>>>
`myDataSource` bean provides all necessary setup for c3p0 (see [documentation][3] for more information) :
```xml
<bean id="myDataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"
        destroy-method="close">
    <property name="driverClass" value="${jdbc.driver.class}" />
    <property name="jdbcUrl" value="${jdbc.url}" />
    <property name="user" value="${jdbc.user}" />
    <property name="password" value="${jdbc.password}" />

    <!-- these are connection pool properties for C3P0 -->
    <property name="minPoolSize" value="${jdbc.min.connections}" />
    <property name="maxPoolSize" value="${jdbc.max.connections}" />
    <property name="maxIdleTime" value="30000" />
    <property name="acquireIncrement" value="${jdbc.acquire.increment}"/>
</bean>
```
Next we configure Hibernate `SessionFactory` as singleton, because its very expensive to create, so (for any given database) the application should have only one associated SessionFactory. The SessionFactory maintains services that Hibernate uses across all Session(s) such as second level caches, connection pools, transaction system integrations, etc:
```xml
<bean id="sessionFactory"
        class="org.springframework.orm.hibernate5.LocalSessionFactoryBean">
    <property name="dataSource" ref="myDataSource"/>
    <property name="packagesToScan" value="academy.softserve.aura.core.entity"/>
    <property name="hibernateProperties">
        <props>
            <prop key="hibernate.dialect">org.hibernate.dialect.PostgreSQL9Dialect</prop>
            <prop key="hibernate.show_sql">true</prop>
        </props>
    </property>
</bean>
```
At last step we configure transactional behaviour for Hibernate:
```xml
<!-- Setup Hibernate transaction manager -->
    <bean id="transactionManager"
        class="org.springframework.orm.hibernate5.HibernateTransactionManager">
    <property name="sessionFactory" ref="sessionFactory"/>
</bean>

<!-- Enable configuration of transactional behavior based on annotations -->
<tx:annotation-driven transaction-manager="transactionManager"/>
```
### 2.3.SecurityConfig.class
**SecurityConfig** enables security via `@EnableWebSecurity` annotation. Next we can configure our security measures thru extending `WebSecurityConfigurerAdapter` and overriding `configure` methods.
For example, next configuration allows all kind for acces for all endpoints:
```java
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);

        http.authorizeRequests()
                .anyRequest().permitAll();
    }
}
```

[1]:http://www.mchange.com/projects/c3p0/#what_is
[2]:https://docs.oracle.com/cd/B28359_01/java.111/e10788/intro.htm#BABHFGCA
[3]:http://www.mchange.com/projects/c3p0/#configuration