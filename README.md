[![build status](https://gitlab.com/kv025java/AuraCore/badges/master/build.svg)](https://gitlab.com/kv025java/AuraCore/commits/master)
[![coverage report](https://gitlab.com/kv025java/AuraCore/badges/master/coverage.svg?job=sonar_qube)](https://gitlab.com/kv025java/AuraCore/commits/master)
# Aura
### Kv-025.Java Project

Aura is a project which helps organizations to monitor the usage of technical devices within the company and also can calculate statistics and perform inventories. 
Managers can provide information about all devices in the storage or that are currently used. This information will act as the basss for the search results displayed to the user. An administrator also uses this application in order to administer the system and keep the information accurate. The administrator can, for instance, give administator's rights to another users and manage user information. Application is created using Spring MVC framework.

#### Visit [Wiki](https://gitlab.com/kv025java/AuraCore/wikis/home) page for detailed documentation
#### [Download](/docs/SRS.docx) SRS

## How to Start Aura Application
1. Run `bash generate_config.sh $DB_HOST $DB_NAME $DB_USER $DB_PASS` to generate DataBase connectivity configuration
2. Run `mvn clean package` to build the application
3. Run `docker build -t AuraCore .` to build the application container
4. Run the following code to launch the application:

```bash
docker network create -d bridge --subnet 172.16.0.0/29 chakra

docker run -d --restart=always --network=chakra -p 5432:5432 -v ./persistence/postgres:/var/lib/postgresql/data \
-e POSTGRES_USER=$DB_USER -e POSTGRES_PASSWORD=$DB_PASS -e POSTGRES_DB=$DB_NAME postgres:latest

docker run -d --restart=always --network=chakra -p 8080:8080 AuraCore
```

## Run entire stack on localhost via Docker

1. Install docker: `curl -sSL get.docker.com | sudo sh`
2. Install python, for ex. on Debian: `sudo apt install python python-pip`
3. Install docker-compose: `sudo pip install docker-compose`
4. Then

```bash
cd $PROJECT_DIR/localhost/
sudo bash compose.sh
```

## Run entire stack on localhost manually

1. Run authentication microservice - [Samsara](https://gitlab.com/kv025java/Samsara). Later use `%YourHost%:9000/login` to authorize after Aura Core started.
2.	Setup environment:
	1. Download [Apache Kafka](http://kafka.apache.org/downloads.html), start Zookeeper and Kafka servers
	2. Setup relational database for Core application and noSQL for microservice (we use PostgreSQL and MongoDB)
3. Run [Aura Core](https://gitlab.com/kv025java/AuraCore):
	1. Setup connection to your local relational database using `AuraCore/src/main/resources/jdbc.properties.example`
	
	3. Run `mvn clean compile`
	4. Deploy project to [Apache Tomcat](https://tomcat.apache.org) or other Servlet container
	5. Check Aura Core API at `%YourHost%:%containerPort%/api/swagger-ui.html` in your browser
4. Run [Karma](https://gitlab.com/kv025java/Karma) microservice:
	1. Generate Aura Core client: in Aura Core run `mvn clean install -Dswagger=client -Dmaven.test.skip=true`
	2. Create `Karma/config.yml` from the `Karma/config.yml.template` (remember server applicationConnectors port)
	3. Setup `bootstrap.servers=%YourHost%:9092` in `Karma\src\main\resources\consumer.properties`
	4. Run `mvn clean install`
	5. Run `java -jar Karma-1.0-SNAPSHOT.jar server config.yml`
	4. Check Karma API at `%YourHost%:%application port%/swagger-ui/` in your browser
	5. Send connection settings for Kafka Producer by Aura Core API `%YourHost%:%AuraPort%/api/swagger-ui.html#!/Setting/addSettingUsingPOST` using json template:
		```
        { "isActive": true, "kafkaHost": "%YourHost%:9092", "kafkaTopicNames": [  "%Karma\src\main\resources\consumer.properties\topic.1%", "%Karma\src\main\resources\consumer.properties\topic.2%" ], "microserviceHost": "%YourHost:%KarmaPort%"}```