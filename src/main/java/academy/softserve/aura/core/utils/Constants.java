package academy.softserve.aura.core.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public final class Constants {


    public static final String USER = "user";

    public static final String RECORDS_COUNT_HEADER = "X-total-records-count";

    public static final String DEPARTMENT_CHANGE_ERROR = "You cannot change department of this user.";
    public static final String ID = "id";
    public static final String USAGE_START_DATE = "usageStartDate";
    public static String JWT_TOKEN_COOKIE_NAME;
    public static String SIGNIN_KEY;
    public static String LOGIN_PATH;

    public static final Long SETTINGS_ID = 1L;

    private Constants() {}


    @Autowired
    public Constants(@Qualifier("superProperties") Properties properties) {
        JWT_TOKEN_COOKIE_NAME = properties.getProperty("JWT_TOKEN_COOKIE_NAME");
        SIGNIN_KEY = properties.getProperty("SIGNIN_KEY");
        LOGIN_PATH = properties.getProperty("loginpath");
    }

}
