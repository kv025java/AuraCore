package academy.softserve.aura.core.swagger.controllers;


import academy.softserve.aura.core.entity.Setting;
import academy.softserve.aura.core.exceptions.AuraException;
import academy.softserve.aura.core.mappers.DtoMapper;
import academy.softserve.aura.core.services.SettingService;
import academy.softserve.aura.core.swagger.model.ErrorDto;
import academy.softserve.aura.core.swagger.model.SettingDto;
import academy.softserve.aura.core.utils.Constants;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
@Validated
public class SettingsApiController implements SettingsApi{

    @Autowired
    private DtoMapper<SettingDto, Setting> settingDtoMapper;

    @Autowired
    private SettingService settingService;

    @Override
    public ResponseEntity<SettingDto> addSetting(
            @ApiParam(value = "Setting object", required = true)
            @RequestBody
                    SettingDto body
    ) {
        Setting entity = settingDtoMapper.toEntity(body);
        entity.setId(Constants.SETTINGS_ID);
        Setting added = settingService.create(entity);

        return new ResponseEntity<>(settingDtoMapper.toDto(added), HttpStatus.OK);
    }


    @Override
    public ResponseEntity<Void> deleteSetting() {
        if (settingService.delete(Constants.SETTINGS_ID)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<SettingDto> getSettings() {
        Setting setting = settingService.findById(Constants.SETTINGS_ID);
        if (setting != null) {
            return new ResponseEntity<>(settingDtoMapper.toDto(setting), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    /**
     * Handler that provides  processing
     *
     * @param e exception to process
     * @return response with {@link ErrorDto}
     */
    @ExceptionHandler({AuraException.class,})
    @ResponseBody
    public ResponseEntity<ErrorDto> handleBadRequest(Exception e) {
        ErrorDto errorDto = new ErrorDto();
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        errorDto.setCode(httpStatus.value());
        errorDto.setMessage(e.getMessage());
        return new ResponseEntity<>(errorDto, httpStatus);
    }

}

