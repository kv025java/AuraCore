package academy.softserve.aura.core.exceptions;

/**
 * Thrown when an exceptional method call occurs
 * in {@link academy.softserve.aura.core.mappers.DtoMapper} mapper type.
 *
 * @author  Eugene Savchenko
 */
public class AuraMapperIllegalMethodException extends AuraException {

    /** Constructs a new {@code AuraMapperIllegalMethodException} with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public AuraMapperIllegalMethodException() {
        super();
    }

    /** Constructs a new {@code AuraMapperIllegalMethodException} with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param   message   the detail message. The detail message is saved for
     *          later retrieval by the {@link #getMessage()} method.
     */
    public AuraMapperIllegalMethodException(String message) {
        super(message);
    }
}
