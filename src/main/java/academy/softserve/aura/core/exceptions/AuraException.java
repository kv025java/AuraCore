package academy.softserve.aura.core.exceptions;

/**
 * Basic class for all exceptions in AuraCore project. Extends {@link RuntimeException}
 *
 * @author Yana Kostiuk
 */
public class AuraException extends RuntimeException {

    /**
     * Constructs a new {@code AuraException} with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public AuraException() {
        super();
    }

    /**
     * Constructs a new {@code AuraException} with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public AuraException(String message) {
        super(message);
    }

}
