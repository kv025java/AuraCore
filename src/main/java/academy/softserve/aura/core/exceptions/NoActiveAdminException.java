package academy.softserve.aura.core.exceptions;


/**
 * Thrown in case of deactivation of user operation when there is only one active admin
 *
 * @author Olexandr Brytskyi
 */

public class NoActiveAdminException extends AuraUserException {


    public NoActiveAdminException() {
        super();
    }

    public NoActiveAdminException(String message) {
        super(message);
    }
}
