package academy.softserve.aura.core.dao;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.User;

import java.util.Collection;

/**
 * The interface which extends basic {@link CrudDao} to work with {@link Department}
 * by adding custom methods.
 *
 * @see Department
 *
 * @author Ivan Vakhovskyi
 */
public interface DepartmentDao extends CrudDao<Department> {

    /**
     * Gets all {@link CommonItem} entities which belongs to specific {@link Department}.
     *
     * @param departmentId the id of {@link Department}
     * @return Collection ({@link java.util.List}) of CommonItems which belongs to specific {@link Department}
     */
    Collection<CommonItem> getDepartmentCommonItems(Long departmentId);

    /**
     * Gets all {@link User} entities who works in specific {@link Department}.
     *
     * @param departmentId the id of {@link Department}
     * @return Collection ({@link java.util.List}) of {@link User} who works in specific {@link Department}
     */
    Collection<User> getDepartmentUsers(Long departmentId);

    /**
     * Gets number of {@link CommonItem} records in database that belong to {@link Department}.
     *
     * @param departmentId the id of {@link Department}
     * @return {@link Long} count of {@link CommonItem} belonging to this department.
     */
    Long getDepartmentCommonItemsCount(Long departmentId);

    /**
     * Gets number of {@link User} records in database that belong to {@link Department}.
     *
     * @param departmentId the id of {@link Department}
     * @return {@link Long} count of {@link User} records belonging to this department.
     */
    Long getDepartmentUsersCount(Long departmentId);

}