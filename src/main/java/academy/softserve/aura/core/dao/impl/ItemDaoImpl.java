package academy.softserve.aura.core.dao.impl;

import academy.softserve.aura.core.dao.ItemDao;
import academy.softserve.aura.core.entity.Item;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.OffsetDateTime;
import java.util.Collection;


@Repository
public class ItemDaoImpl extends CrudDaoImpl<Item> implements ItemDao {

    @Autowired
    private SessionFactory sessionFactory;

    public ItemDaoImpl() {
        super(Item.class);
    }

    @Override
    public Collection<Item> findFewByIsWorking(int offset,
                                               int limit,
                                               boolean isWorking){
        Session session;
        Collection<Item> items;
            session = sessionFactory.getCurrentSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Item> criteriaQuery = builder.createQuery(Item.class);
            Root<Item> itemRoot = criteriaQuery.from(Item.class);
            criteriaQuery.select(itemRoot)
                    .where(builder.equal(itemRoot.get("working"),isWorking));
            items = session.createQuery(criteriaQuery)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();
        return items;
    }

    @Override
    public Long getItemsFilteredByIsWorkingCount(boolean isWorking) {
        Long itemsCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<Item> itemRoot = criteriaQuery.from(Item.class);

        criteriaQuery
                .select(builder.count(itemRoot))
                .where(builder.equal(itemRoot.get("working"),isWorking));

        itemsCount = session.createQuery(criteriaQuery).getSingleResult();

        return itemsCount;
    }

    @Override
    public boolean assignItemToUser(Item item, Long userId) {
        item.getComponents().forEach(item1 -> assignItemToUser(item1, userId));
        Session session = sessionFactory.getCurrentSession();
        OffsetDateTime dateTime = OffsetDateTime.now();
        session.createNativeQuery("insert into private_item values (:user_id, :item_id, :usage_start_date)")
                .setParameter("user_id", userId)
                .setParameter("item_id", item.getId())
                .setParameter("usage_start_date", dateTime)
                .executeUpdate();
        return true;
    }

    @Override
    public boolean assignItemToDepartment(Item item, Long departmentId) {
        item.getComponents().forEach(item1 -> assignItemToDepartment(item1, departmentId));
        Session session = sessionFactory.getCurrentSession();
        session.createNativeQuery("insert into common_item values (:department_id, :item_id)")
                .setParameter("department_id", departmentId)
                .setParameter("item_id", item.getId())
                .executeUpdate();
        return true;
    }
}
