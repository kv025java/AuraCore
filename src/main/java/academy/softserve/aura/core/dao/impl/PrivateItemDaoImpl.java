package academy.softserve.aura.core.dao.impl;

import academy.softserve.aura.core.dao.PrivateItemDao;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.Collection;

@Repository
public class PrivateItemDaoImpl extends CrudDaoImpl<PrivateItem> implements PrivateItemDao {

    public PrivateItemDaoImpl() {
        super(PrivateItem.class);
    }

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Collection<PrivateItem> findFewByIsWorking(int offset, int limit, boolean isWorking) {
        Collection<PrivateItem> privateItems;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<PrivateItem> criteriaQuery = builder.createQuery(PrivateItem.class);
        Root<PrivateItem> privateItemRoot = criteriaQuery.from(PrivateItem.class);

        criteriaQuery
                .select(privateItemRoot)
                .where(builder.equal(privateItemRoot.get("working"),isWorking));

        privateItems = session.createQuery(criteriaQuery)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return privateItems;
    }

    @Override
    public Collection<User> getPrivateItemUsers(Long itemID) {
        Collection<User> users;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
        Root<PrivateItem> privateItemRoot = criteriaQuery.from(PrivateItem.class);
        Join<PrivateItem, User> userJoin = privateItemRoot.join("allUsersWhoUsedItem");

        criteriaQuery
                .select(userJoin)
                .where(builder.equal(privateItemRoot.get("id"), itemID));

        users = session.createQuery(criteriaQuery).getResultList();

        return users;
    }

    @Override
    public Long getPrivateItemsFilteredByIsWorkingCount(boolean isWorking) {
        Long itemsCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<PrivateItem> itemRoot = criteriaQuery.from(PrivateItem.class);

        criteriaQuery
                .select(builder.count(itemRoot))
                .where(builder.equal(itemRoot.get("working"),isWorking));

        itemsCount = session.createQuery(criteriaQuery).getSingleResult();

        return itemsCount;
    }

    @Override
    public boolean assignPrivateItemToWarehouse(PrivateItem privateItem) {
        privateItem.getComponents().forEach(item -> assignPrivateItemToWarehouse((PrivateItem) item));
        Session session = sessionFactory.getCurrentSession();
        session.createNativeQuery("delete from private_item where item_id = :item_id")
                .setParameter("item_id", privateItem.getId())
                .executeUpdate();
        return true;
    }

}
