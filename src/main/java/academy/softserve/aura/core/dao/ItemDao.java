package academy.softserve.aura.core.dao;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.PrivateItem;

import java.util.Collection;

/**
 * The basic interface to work with {@link Item} entities on their abstract level.
 * It is extends  basic {@link CrudDao}
 *
 * @see Item
 *
 * @author Ivan Vakhovskyi
 */
public interface ItemDao extends CrudDao<Item> {

    /**
     * Gets a chosen number of {@link Item} from collection by their working status.
     *
     * @param offset the point of first entry to return from a collection
     * @param limit  the number of entries to return from a collection
     * @param isWorking the status of Items to be fetched from a DB
     * @return Collection ({@link java.util.List}) of {@link Item} which satisfy parameters
     */
    Collection<Item> findFewByIsWorking(int offset, int limit, boolean isWorking);

    /**
     * Gets number of {@link Item} records in database filtered by working status.
     *
     * @param isWorking the status of Items to be fetched from a DB
     * @return {@link Long} count of records in database filtered by working status.
     */
    Long getItemsFilteredByIsWorkingCount(boolean isWorking);

    /**
     * Gets {@link Item} and change it to {@link PrivateItem}
     *
     * @param item what will be converted to {@link PrivateItem}
     * @param userId id of User who will hold {@link PrivateItem}
     * @return true if item was assign to User
     */
    boolean assignItemToUser (Item item, Long userId);

    /**
     * Gets {@link Item} and change it to {@link CommonItem}
     *
     * @param item what will be converted to {@link CommonItem}
     * @param departmentId id of Department which will hold {@link CommonItem}
     * @return true if item was assign to Department
     */
    boolean assignItemToDepartment (Item item, Long departmentId);

}
