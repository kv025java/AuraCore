package academy.softserve.aura.core.dao;


import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.ItemLog;
import academy.softserve.aura.core.services.ItemLogService;

import java.time.OffsetDateTime;
import java.util.Collection;

/**
 * The basic interface for CRUD operations with {@link ItemLog}.
 * Extended with few specific methods.
 *
 * Actually, we won't use UPDATE and DELETE operations for ItemLogs now.
 * This restriction is handled by {@link ItemLogService}
 *
 */
public interface ItemLogDao extends CrudDao<ItemLog> {
    /**
     * SELECT operation for all event logs since some date.
     *
     * @param sinceTime start time
     * @return the collection of entities from DB
     */
    Collection<ItemLog> getAllSinceTime(OffsetDateTime sinceTime);

    /**
     * SELECT operation for all event logs by {@link Item} id.
     *
     * @param id of the {@link Item}
     * @return the collection of entities from DB
     */
    Collection<ItemLog> getAllByItemId(Long id);

    /**
     * SELECT operation for all event logs by {@link Item} id and since some date
     *
     * @param id of the {@link Item}
     * @param sinceTime start time
     * @return the collection of entities from DB
     */
    Collection<ItemLog> getAllByItemIdAndTime(Long id, OffsetDateTime sinceTime);
}
