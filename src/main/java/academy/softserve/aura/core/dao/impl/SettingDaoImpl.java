package academy.softserve.aura.core.dao.impl;

import academy.softserve.aura.core.dao.SettingDao;
import academy.softserve.aura.core.entity.Setting;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
public class SettingDaoImpl extends CrudDaoImpl<Setting>  implements SettingDao {

    public SettingDaoImpl() {
        super(Setting.class);
    }

    @Override
    public Setting getElementByID(Long elementId) {
        Setting setting;
        try (Session session = sessionFactory.openSession()) {
            setting = session.get(Setting.class, elementId);
            if (setting != null) {
                session.evict(setting);
            }
        }
        return setting;
    }

    @Override
    public Setting addElement(Setting element) {
        Session session = sessionFactory.getCurrentSession();
        session.save(element);
        return element;
    }

    @Override
    public Setting updateElement(Setting element) {
        Session session = sessionFactory.getCurrentSession();
        session.merge(element);
        return super.updateElement(element);
    }
}
