package academy.softserve.aura.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "item")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name="isworking", nullable = false)
    protected boolean working;

    @Column(name="manufacturer", nullable = false)
    protected String manufacturer;

    @Column(name="model", nullable = false)
    protected String model;

    @Column(name="description", nullable = false)
    protected String description;

    @Column(name="start_price", nullable = false)
    protected BigDecimal startPrice;

    @Column(name="current_price", nullable = false)
    protected BigDecimal currentPrice;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "parentItem")
    protected List<Item> components;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_item_id", referencedColumnName = "id")
    protected Item parentItem;

    @Column(name="nominal_resource", nullable = false)
    protected double nominalResource;

    @Column(name = "manufacture_date", nullable = false)
    protected OffsetDateTime manufactureDate;

    @Column(name = "aging_factor", nullable = false)
    protected double agingFactor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public List<Item> getComponents() {
        return components;
    }

    public void setComponents(List<Item> components) {
        this.components = components;
    }

    public Item getParentItem() {
        return parentItem;
    }

    public void setParentItem(Item parentItem) {
        this.parentItem = parentItem;
    }

    public double getNominalResource() {
        return nominalResource;
    }

    public void setNominalResource(double nominalResource) {
        this.nominalResource = nominalResource;
    }

    public OffsetDateTime getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(OffsetDateTime manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public double getAgingFactor() {
        return agingFactor;
    }

    public void setAgingFactor(double agingFactor) {
        this.agingFactor = agingFactor;
    }
}
