package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserCommonItemEvent;
import academy.softserve.aura.core.swagger.model.UserCommonItemEventDto;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;

/**
 * The mapper class to perform mapping between
 * {@link UserCommonItemEvent} entities and {@link UserCommonItemEventDto} DTO objects.
 * It implements basic {@link DtoMapper}.
 *
 * @see UserCommonItemEvent
 */
@Component
public class UserCommonItemEventMapper implements DtoMapper<UserCommonItemEventDto, UserCommonItemEvent> {

    /**
     * Gets {@link UserCommonItemEvent} entity and maps them to specific DTO: {@link UserCommonItemEventDto}.
     *
     * @param entity the the entity {@link UserCommonItemEvent} type to be mapped to DTO object
     * @return mapped DTO object of type {@link UserCommonItemEventDto}
     */
    @Override
    public UserCommonItemEventDto toDto(UserCommonItemEvent entity) {
        if (entity == null) {
            return null;
        }

        UserCommonItemEventDto userCommonItemEventDto = new UserCommonItemEventDto();

        Long userId = entityUserId(entity);
        if (userId != null) {
            userCommonItemEventDto.setUserId(userId);
        }
        userCommonItemEventDto.setCommonItemId(entity.getCommonItemId());
        userCommonItemEventDto.setId(entity.getId());
        userCommonItemEventDto.setUsageStartDate(entity.getUsageStartDate());
        userCommonItemEventDto.setUsageEndDate(entity.getUsageEndDate());
        userCommonItemEventDto.setUsageDuration(entity.getUsageDuration());

        return userCommonItemEventDto;
    }

    /**
     * @param dto incoming DTO object to map
     */
    @Override
    public UserCommonItemEvent toEntity(UserCommonItemEventDto dto) {
        UserCommonItemEvent entity = new UserCommonItemEvent();
        CommonItem item = new CommonItem();
        User user = new User();
        double duration = ChronoUnit.MINUTES.between(dto.getUsageStartDate(), dto.getUsageEndDate());

        item.setId(dto.getCommonItemId());
        user.setId(dto.getUserId());

        entity.setId(dto.getId());
        entity.setCommonItemId(dto.getCommonItemId());
        entity.setUsageDuration(duration);
        entity.setUsageStartDate(dto.getUsageStartDate());
        entity.setUsageEndDate(dto.getUsageEndDate());
        entity.setUser(user);

        return entity;
    }

    /**
     * Provides user's id for current event object
     * @param userCommonItemEvent event object for which userId is needed
     * @return users's ID
     */
    private static Long entityUserId(UserCommonItemEvent userCommonItemEvent) {
        User user = userCommonItemEvent.getUser();

        if (user == null || user.getId() == null) {
            return null;
        } else {
            return user.getId();
        }
    }

}
