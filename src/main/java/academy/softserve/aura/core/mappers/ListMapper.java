package academy.softserve.aura.core.mappers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Generic mapper class to perform mapping between {@link List} of entities and DTO objects.
 * Uses {@link DtoMapper} of corresponding type.
 *@param <E>  the type of Entity
 *@param <D> the type of corresponding DTO object
 */
class ListMapper<D, E> {


    private final DtoMapper<D, E> mapper;

    /**
     * Constructor
     * @param mapper a mapper that will provide mapping operations on elements of list.
     *               Type parameters must correspond to list types.
     */
    ListMapper(DtoMapper<D, E> mapper) {
        this.mapper = mapper;
    }

    /**
     * Gets List of entities and maps them to specific list of DTOs.
     *
     * @param entityList the the entities List to be mapped.
     * @return mapped DTOs List object of  specified type.
     */
    List<D> toDtoList(List<E> entityList) {
        if (entityList != null && !entityList.isEmpty()) {
            List<D> dtoList = new ArrayList<>(entityList.size());
            dtoList.addAll(entityList.stream().map(mapper::toDto).collect(Collectors.toList()));
            return dtoList;
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Gets List of DTO and maps them to specific list of entities.
     *
     * @param dtoList the the entities List to be mapped.
     * @return mapped DTOs List object of specified type.
     */
    List<E> toEntityList(List<D> dtoList) {
        if (dtoList != null && !dtoList.isEmpty()) {
            List<E> entityList = new ArrayList<>(dtoList.size());
            entityList.addAll(dtoList.stream().map(mapper::toEntity).collect(Collectors.toList()));
            return entityList;
        } else {
            return new ArrayList<>();
        }
    }
}
