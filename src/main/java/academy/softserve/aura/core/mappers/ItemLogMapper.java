package academy.softserve.aura.core.mappers;


import academy.softserve.aura.core.entity.ItemLog;
import academy.softserve.aura.core.exceptions.AuraMapperIllegalMethodException;
import academy.softserve.aura.core.swagger.model.ItemLogDto;
import org.springframework.stereotype.Component;

/**
 * The mapper class to perform mapping between
 * {@link ItemLog} entities and {@link ItemLogDto} DTO objects.
 * It implements basic {@link DtoMapper}.
 *
 * @see ItemLog
 */
@Component
public class ItemLogMapper implements DtoMapper<ItemLogDto, ItemLog> {
    
    /**
     * Gets {@link ItemLog} entity and maps them to specific DTO: {@link ItemLogDto}.
     *
     * @param entity the the entity {@link ItemLog} type to be mapped to DTO object
     * @return mapped DTO object of type {@link ItemLogDto}
     */
    @Override
    public ItemLogDto toDto(ItemLog entity) {
        if (entity == null) {
            return null;
        }

        ItemLogDto itemLogDto = new ItemLogDto();

        if (entity.getEventType() != null) {
            itemLogDto.setItemEventType(entity.getEventType().toString());
        }
        itemLogDto.setId(entity.getId());
        itemLogDto.setItemId(entity.getItemId());
        itemLogDto.setEventDate(entity.getEventDate());
        itemLogDto.setDescription(entity.getDescription());

        return itemLogDto;
    }

    /**
     * @param dto incoming DTO object to map
     * @exception  AuraMapperIllegalMethodException
     * @deprecated Must not be used because there is no DTO objects of parameter type
     */
    @Deprecated
    @Override
    public ItemLog toEntity(ItemLogDto dto) {
        throw new AuraMapperIllegalMethodException("This method is unsupported for current object");
    }
}
