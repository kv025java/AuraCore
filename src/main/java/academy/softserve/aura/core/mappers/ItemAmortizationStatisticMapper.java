package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.ItemAmortizationStatistic;
import academy.softserve.aura.core.swagger.model.ItemAmortizationStatisticDto;
import org.springframework.stereotype.Component;

@Component
public class ItemAmortizationStatisticMapper implements DtoMapper<ItemAmortizationStatisticDto, ItemAmortizationStatistic>{

    @Override
    public ItemAmortizationStatisticDto toDto(ItemAmortizationStatistic entity) {
        if (entity == null) {
            return null;
        }

        ItemAmortizationStatisticDto itemAmortizationStatisticDto = new ItemAmortizationStatisticDto();

        itemAmortizationStatisticDto.setCurrentPrice(entity.getCurrentPrice());
        itemAmortizationStatisticDto.setValueOfMonthAmortization(entity.getValueOfMonthAmortization());
        itemAmortizationStatisticDto.setMonthsOfUse(entity.getMonthOfUse());

        return itemAmortizationStatisticDto;
    }

    @Override
    public ItemAmortizationStatistic toEntity(ItemAmortizationStatisticDto dto) {
        if (dto == null) {
            return null;
        }

        ItemAmortizationStatistic itemAmortizationStatistic = new ItemAmortizationStatistic();

        itemAmortizationStatistic.setCurrentPrice(dto.getCurrentPrice());
        itemAmortizationStatistic.setValueOfMonthAmortization(dto.getValueOfMonthAmortization());
        itemAmortizationStatistic.setMonthOfUse(dto.getMonthsOfUse());

        return itemAmortizationStatistic;
    }
}
