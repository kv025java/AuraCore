package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Setting;
import academy.softserve.aura.core.swagger.model.SettingDto;
import org.springframework.stereotype.Component;

@Component
public class SettingMapper implements DtoMapper<SettingDto, Setting>{
    @Override
    public SettingDto toDto(Setting entity) {
        if (entity == null) {
            return null;
        }

        SettingDto settingDto = new SettingDto();

        settingDto.setId(entity.getId());
        settingDto.setIsActive(entity.isActive());
        settingDto.setKafkaHost(entity.getKafkaHost().trim());
        settingDto.setKafkaTopicNames(entity.getKafkaTopicNames());
        settingDto.setMicroserviceHost(entity.getMicroserviceHost());

        return settingDto;
    }

    @Override
    public Setting toEntity(SettingDto dto) {
        if (dto == null) {
            return null;
        }

        Setting setting = new Setting();

        setting.setId(dto.getId());
        setting.setActive(dto.getIsActive());
        setting.setKafkaHost(dto.getKafkaHost());
        setting.setKafkaTopicNames(dto.getKafkaTopicNames());
        setting.setMicroserviceHost(dto.getMicroserviceHost());

        return setting;
    }
}
