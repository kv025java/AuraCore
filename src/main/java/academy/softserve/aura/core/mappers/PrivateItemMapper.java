package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.swagger.model.PrivateItemDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The mapper class to perform mapping between
 * {@link PrivateItem} entities and {@link PrivateItemDto} DTO objects.
 * It implements basic {@link DtoMapper} and uses customized instance of {@link ListMapper}.
 *
 * @see PrivateItem
 */
@Component
public class PrivateItemMapper implements DtoMapper<PrivateItemDto, PrivateItem> {
    private ListMapper<PrivateItemDto, Item> privateItemListMapper;

    /**
     * Constructor
     * Initializes ListMapper
     */
    public PrivateItemMapper() {
        privateItemListMapper = new ListMapper<PrivateItemDto, Item>(null) {

            @Override
            List<PrivateItemDto> toDtoList(List<Item> entityList) {
                if (entityList != null && !entityList.isEmpty()) {
                    PrivateItemMapper privateItemMapper = new PrivateItemMapper();
                    List<PrivateItemDto> dtoList = new ArrayList<>(entityList.size());
                    dtoList.addAll(entityList.stream().map(entity -> privateItemMapper.toDto((PrivateItem) entity)).collect(Collectors.toList()));
                    return dtoList;
                } else {
                    return new ArrayList<>();
                }
            }

            @Override
            List<Item> toEntityList(List<PrivateItemDto> dtoList) {
                if (dtoList != null && !dtoList.isEmpty()) {
                    PrivateItemMapper privateItemMapper = new PrivateItemMapper();
                    List<Item> entityList = new ArrayList<>(dtoList.size());
                    entityList.addAll(dtoList.stream().map(privateItemMapper::toEntity).collect(Collectors.toList()));
                    return entityList;
                } else {
                    return new ArrayList<>();
                }
            }
        };
    }

    /**
     * Gets {@link PrivateItem} entity and maps them to specific DTO: {@link PrivateItemDto}.
     *
     * @param entity the the entity {@link PrivateItem} type to be mapped to DTO object
     * @return mapped DTO object of type {@link PrivateItemDto}
     */
    @Override
    public PrivateItemDto toDto(PrivateItem entity) {
        if (entity == null) {
            return null;
        }

        PrivateItemDto privateItemDto = new PrivateItemDto();

        Long id = entityOwnerId(entity);
        if (id != null) {
            privateItemDto.setUserId(id);
        }
        privateItemDto.setIsWorking(entity.isWorking());
        privateItemDto.setId(entity.getId());
        privateItemDto.setManufacturer(entity.getManufacturer());
        privateItemDto.setModel(entity.getModel());
        privateItemDto.setStartPrice(entity.getStartPrice());
        privateItemDto.setCurrentPrice(entity.getCurrentPrice());
        privateItemDto.setNominalResource(entity.getNominalResource());
        privateItemDto.setDescription(entity.getDescription());
        privateItemDto.setUsageStartDate(entity.getUsageStartDate());
        privateItemDto.setComponents(privateItemListMapper.toDtoList(entity.getComponents()));
        privateItemDto.setItemType(entity.getClass().getSimpleName());
        privateItemDto.setManufactureDate(entity.getManufactureDate());
        privateItemDto.setAgingFactor(entity.getAgingFactor());

        return privateItemDto;
    }

    /**
     * Gets {@link PrivateItemDto} DTO object and maps them to specific entity: {@link PrivateItem}.
     *
     * @param dto the DTO {@link PrivateItemDto} type to be mapped to DTO object
     * @return new entity object of type {@link PrivateItem}
     */
    @Override
    public PrivateItem toEntity(PrivateItemDto dto) {
        if (dto == null) {
            return null;
        }

        PrivateItem privateItem = new PrivateItem();

        privateItem.setOwner(privateItemDtoToUser(dto));

        if (dto.getIsWorking() != null) {
            privateItem.setWorking(dto.getIsWorking());
        }

        privateItem.setId(dto.getId());
        privateItem.setManufacturer(dto.getManufacturer());
        privateItem.setModel(dto.getModel());
        privateItem.setDescription(dto.getDescription());
        privateItem.setStartPrice(dto.getStartPrice());
        privateItem.setCurrentPrice(dto.getCurrentPrice());
        privateItem.setComponents(privateItemListMapper.toEntityList(dto.getComponents()));

        if (!privateItem.getComponents().isEmpty()) {
            privateItem.getComponents().forEach((u -> u.setParentItem(privateItem)));
        }

        if (dto.getNominalResource() != null) {
            privateItem.setNominalResource(dto.getNominalResource());
        }

        privateItem.setUsageStartDate(dto.getUsageStartDate());
        privateItem.setManufactureDate(dto.getManufactureDate());
        privateItem.setAgingFactor(dto.getAgingFactor());

        return privateItem;
    }

    private static Long entityOwnerId(PrivateItem privateItem) {
        User owner = privateItem.getOwner();

        if (owner == null || owner.getId() == null) {
            return null;
        } else {
            return owner.getId();
        }
    }

    private static User privateItemDtoToUser(PrivateItemDto privateItemDto) {
        User user = new User();
        user.setId(privateItemDto.getUserId());
        return user;
    }
}
