package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.swagger.model.CommonItemDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The mapper class to perform mapping between
 * {@link CommonItem} entities and {@link CommonItemDto} DTO objects.
 * It implements basic {@link DtoMapper} and uses customized instance of {@link ListMapper}.
 *
 * @see CommonItem
 */
@Component
public class CommonItemMapper implements DtoMapper<CommonItemDto, CommonItem> {

    /*
        Customized ListMapper instance without default <CommonItemDto, Item> mapper.
        Dedicated versions of CommonItemMapper are created in methods.
    */
    private ListMapper<CommonItemDto, Item> commonItemListMapper;

    /**
     * Constructor
     * Initializes ListMapper
     */
    public CommonItemMapper() {
        commonItemListMapper = new ListMapper<CommonItemDto, Item>(null) {
            @Override
            List<CommonItemDto> toDtoList(List<Item> entityList) {
                if (entityList != null && !entityList.isEmpty()) {
                    CommonItemMapper commonItemMapper = new CommonItemMapper();
                    List<CommonItemDto> dtoList = new ArrayList<>(entityList.size());
                    dtoList.addAll(entityList.stream().map(entity -> commonItemMapper.toDto((CommonItem) entity)).collect(Collectors.toList()));
                    return dtoList;
                } else {
                    return new ArrayList<>();
                }
            }

            @Override
            List<Item> toEntityList(List<CommonItemDto> dtoList) {
                if (dtoList != null && !dtoList.isEmpty()) {
                    CommonItemMapper commonItemMapper = new CommonItemMapper();
                    List<Item> entityList = new ArrayList<>(dtoList.size());
                    entityList.addAll(dtoList.stream().map(commonItemMapper::toEntity).collect(Collectors.toList()));
                    return entityList;
                } else {
                    return new ArrayList<>();
                }
            }
        };
    }

    /**
     * Gets {@link CommonItem} entity and maps them to specific DTO: {@link CommonItemDto}.
     *
     * @param entity the the entity {@link CommonItem} type to be mapped to DTO object
     * @return mapped DTO object of type {@link CommonItemDto}
     */
    @Override
    public CommonItemDto toDto(CommonItem entity) {
        if (entity == null) {
            return null;
        }

        CommonItemDto commonItemDto = new CommonItemDto();

        commonItemDto.setIsWorking(entity.isWorking());
        Long id = entityDepartmentId(entity);
        if (id != null) {
            commonItemDto.setDepartmentId(id);
        }
        commonItemDto.setId(entity.getId());
        commonItemDto.setManufacturer(entity.getManufacturer());
        commonItemDto.setModel(entity.getModel());
        commonItemDto.setStartPrice(entity.getStartPrice());
        commonItemDto.setCurrentPrice(entity.getCurrentPrice());
        commonItemDto.setNominalResource(entity.getNominalResource());
        commonItemDto.setDescription(entity.getDescription());
        commonItemDto.setComponents(commonItemListMapper.toDtoList(entity.getComponents()));
        commonItemDto.setItemType(entity.getClass().getSimpleName());
        commonItemDto.setManufactureDate(entity.getManufactureDate());
        commonItemDto.setAgingFactor(entity.getAgingFactor());

        return commonItemDto;
    }

    /**
     * Gets {@link CommonItemDto} DTO object and maps them to specific entity: {@link CommonItem}.
     *
     * @param dto the DTO {@link CommonItemDto} type to be mapped to DTO object
     * @return new entity object of type {@link CommonItem}
     */
    @Override
    public CommonItem toEntity(CommonItemDto dto) {
        if (dto == null) {
            return null;
        }

        CommonItem commonItem = new CommonItem();

        if (dto.getDepartmentId() == null) {
            commonItem.setDepartment(null);
        } else {
            Department department = new Department();
            department.setId(dto.getDepartmentId());
            commonItem.setDepartment(department);
        }

        if (dto.getIsWorking() != null) {
            commonItem.setWorking(dto.getIsWorking());
        }

        commonItem.setId(dto.getId());
        commonItem.setManufacturer(dto.getManufacturer());
        commonItem.setModel(dto.getModel());
        commonItem.setDescription(dto.getDescription());
        commonItem.setStartPrice(dto.getStartPrice());
        commonItem.setCurrentPrice(dto.getCurrentPrice());
        commonItem.setComponents(commonItemListMapper.toEntityList(dto.getComponents()));

        if (!commonItem.getComponents().isEmpty()){
            commonItem.getComponents().forEach((u -> u.setParentItem(commonItem)));
        }

        if (dto.getNominalResource() != null) {
            commonItem.setNominalResource(dto.getNominalResource());
        }

        commonItem.setManufactureDate(dto.getManufactureDate());
        commonItem.setAgingFactor(dto.getAgingFactor());

        return commonItem;
    }

    /**
     * Gets ID of department item belongs to
     * @param commonItem {@link CommonItem} for which department id is returned
     * @return id of items's department
     */
    private static Long entityDepartmentId(CommonItem commonItem) {
        Department department = commonItem.getDepartment();

        if (department == null || department.getId() == null) {
            return null;
        } else {
            return department.getId();
        }
    }
}
