package academy.softserve.aura.core.security;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URI;

import static academy.softserve.aura.core.utils.Constants.LOGIN_PATH;

/**
 * Bean just for forwarding to login page or redirecting to SSO server
 */
@Controller
public class LoginEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(LoginEndpoint.class);


    /**
     * simply redirects user to SSO server login page
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ResponseEntity showLoginPage() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && !authentication.getName().equals("anonymousUser") && authentication.isAuthenticated())
            throw new SessionAuthenticationException("Already authenticated! Logout first");
        return ResponseEntity.status(HttpStatus.TEMPORARY_REDIRECT).location(URI.create(LOGIN_PATH)).build();
    }

    /**
     * makes user to send parameters via same {@link org.springframework.http.HttpMethod#POST}
     * to SSO authorization server endpoint
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Void> login(
            @ApiParam(value = "login", required = true)
            @RequestParam(value = "login")
                    String login,
            @ApiParam(value = "password", required = true)
            @RequestParam(value = "password")
                    String password) {

        logger.info("User with login {} want to log in", login);
        return ResponseEntity.status(HttpStatus.TEMPORARY_REDIRECT).location(URI.create(LOGIN_PATH)).build();

    }
}
