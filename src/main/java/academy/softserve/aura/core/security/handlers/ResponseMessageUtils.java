package academy.softserve.aura.core.security.handlers;


import org.springframework.http.HttpStatus;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Holds additional methods to work with response
 */
public final class ResponseMessageUtils {

    /**
     * write
     *
     * @param message  to
     * @param response with status
     * @param status
     */
    public static void sendResponseMessage(HttpServletResponse response, HttpStatus status, String message) throws IOException {
        try (ServletOutputStream outputStream = response.getOutputStream()) {
            response.setStatus(status.value());
            outputStream.print(message);

        }
    }
}
