package academy.softserve.aura.core.security;

import academy.softserve.aura.core.security.jwt.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


/**
 * Base configuration for Spring Security
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ADMIN = "ADMIN";
    private static final String MANAGER = "MANAGER";
    private static final String USER = "USER";

    /**
     * filter which will check tokens
     */
    private final JwtFilter jwtFilter;

    /**
     * used for hashing passwords
     * need to use it when saving new {@link academy.softserve.aura.core.entity.User}
     */
    private final PasswordEncoder passwordEncoder;

    /**
     * handler will work in case user does not have permissions to visit some endpoint
     */
    private final AccessDeniedHandler accessDeniedHandler;

    @Autowired
    public SecurityConfig(PasswordEncoder passwordEncoder, AccessDeniedHandler accessDeniedHandler, JwtFilter jwtFilter) {
        this.passwordEncoder = passwordEncoder;
        this.accessDeniedHandler = accessDeniedHandler;
        this.jwtFilter = jwtFilter;
    }

    /**
     * add view resolver to context
     * such a way in controllers you should just return page name
     */
    @Bean
    public InternalResourceViewResolver htmlViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/pages/");
        resolver.setSuffix(".html");
        return resolver;
    }


    /**
     * ignore securing resources, which should be opened for all
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/**/resources/**")
                .antMatchers("/**/swagger-resources/**")
                .antMatchers("/**/webjars/**")
                .antMatchers("/**/api-docs**")
                .antMatchers("/**/api/swagger-ui.html")
                .antMatchers("/assets/**");

    }

    /**
     * allow to configure {@link HttpSecurity} with our rules
     * JWT authorization is mostly implemented by adding {@link JwtFilter}
     *
     * @param http the {@link HttpSecurity} to modify
     * @see HttpSecurity
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requestMatchers()
                .antMatchers("/api/**")
                .and()

                .authorizeRequests()

                .antMatchers("/api/departments").hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.GET, "/api/departments/**").hasAnyRole(ADMIN, MANAGER)
                .antMatchers(HttpMethod.PUT, "/api/departments/**").hasAnyRole(ADMIN, MANAGER)
                .antMatchers(HttpMethod.POST, "/api/departments").hasAnyRole(ADMIN)
                .antMatchers(HttpMethod.DELETE, "/api/departments/**").hasAnyRole(ADMIN)

                .antMatchers(HttpMethod.PUT, "api/items/private/**").hasAnyRole(ADMIN, MANAGER)
                .antMatchers(HttpMethod.PUT, "api/items/common/**").hasAnyRole(ADMIN, MANAGER)
                .antMatchers(HttpMethod.PUT, "api/items/**").hasAnyRole(ADMIN, MANAGER)
                .antMatchers("/api/items/**").hasAnyRole(USER, ADMIN, MANAGER)

                .antMatchers(HttpMethod.GET, "/api/users/**").hasAnyRole(USER, ADMIN, MANAGER)
                .antMatchers(HttpMethod.PUT, "/api/users/**").hasAnyRole(USER, ADMIN, MANAGER)
                .antMatchers(HttpMethod.POST, "/api/users/**").hasRole(ADMIN)

                .antMatchers(HttpMethod.GET, "/api/logs/item/common").hasAnyRole(ADMIN, MANAGER)
                .antMatchers(HttpMethod.POST, "/api/logs/item/common").hasAnyRole(USER, ADMIN, MANAGER)
                .antMatchers("/api/login**", "/api/logout**", "/api/logoutFull**").permitAll()
                .anyRequest().fullyAuthenticated()

                .and()
                .csrf().disable()
                .addFilterAfter(jwtFilter, AbstractPreAuthenticatedProcessingFilter.class)
                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler);
    }

}