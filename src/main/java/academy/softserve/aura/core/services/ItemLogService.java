package academy.softserve.aura.core.services;


import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.entity.ItemLog;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

public interface ItemLogService extends CrudService<ItemLog> {

    ItemLog createAndSaveSimpleItemLog(String itemId, OffsetDateTime dateTime, ItemEventType eventType, String message);

    /**
     * Method gets all {@link ItemLog} which were created since some time
     *
     * @param sinceTime is the log's time from which they will be retrieved
     * @return Collection of {@link ItemLog}
     */
    public Collection<ItemLog> getAllSinceTime(OffsetDateTime sinceTime);

    /**
     * Method gets all {@link ItemLog} for specific {@link Item}
     *
     * @param id of the {@link Item}
     * @return Collection of {@link ItemLog}
     */
    public Collection<ItemLog> getAllByItemId(Long id);

    /**
     * Method gets all {@link ItemLog} which were created since some time for specific {@link Item}
     *
     * @param id
     * @param sinceTime
     * @return Collection of {@link ItemLog}
     */
    public Collection<ItemLog> getAllByItemIdAndTime(Long id, OffsetDateTime sinceTime);

    /**
     * Recursively makes ITEM_CREATED logs for parent {@link Item} and every new component in its down hierarchy
     *
     * @param item is current (parent) item
     * @param logList is where all current logs are stored
     * @param upgradedItems is where all current logs for UPDATED (not new) items are stored
     */
    void generateCreatedLogsForHierarchy(Item item, List<ItemLog> logList, List<String> upgradedItems);

    /**
     * Recursively makes ITEM_UPDATED logs for parent {@link Item} and every component in up hierarchy
     *
     * @param item is entity of parent {@link Item}
     * @param logList is where all current logs are stored
     */
    void generateUpgradedLogsForHierarchy(Item item, List<ItemLog> logList);

    /**
     * Recursively makes ITEM_UPDATED logs for every parent {@link Item} in hierarchy
     *
     * @param item is updated (child) item
     */
    void generateAndSaveUpdateLogsForParent(Item item,List<ItemLog> logList);
}
