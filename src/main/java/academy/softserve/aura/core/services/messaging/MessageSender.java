package academy.softserve.aura.core.services.messaging;

/**
 * Simple interface for messaging
 */
public interface MessageSender {
    /**
     * Method to send message via specific message producer
     *
     * @param message is the {@link Object} to be send
     */
    void sendMessage(Object message);
}
