package academy.softserve.aura.core.services;


import academy.softserve.aura.core.entity.*;
import academy.softserve.aura.core.services.messaging.MessageSender;

import java.util.Collection;

/**
 * The interface to work with {@link CommonItem} entities.
 * It is extends basic {@link CrudService}.
 *
 * @see Item
 * @see CommonItem
 */
public interface CommonItemService extends CrudService<CommonItem> {

    /**
     * Gets few {@link CommonItem} from collection by their working status.
     *
     * @param offset the first entry to return from a collection
     * @param limit  the quantity of entries to return from a collection
     * @param isWorking the status of Items to be fetched from a DB
     * @return Collection ({@link java.util.List}) of {@link Item} which satisfy parameters
     */
    Collection<CommonItem> findFewByIsWorking(int offset, int limit, boolean isWorking);


    /**
     * Gets all {@link Department} entities which used specific {@link CommonItem}.
     *
     * @param id the id of {@link CommonItem}
     * @return Collection ({@link java.util.List}) of {@link Department} which used specific {@link CommonItem}
     */
    Collection<Department> getItemDepartments(Long id);

    /**
     * Gets number of {@link CommonItem} records in database.
     *
     * @return {@link Long} count of records in database.
     */
    Long getCommonItemsCount();

    /**
     * Gets number of {@link CommonItem} records in database filtered by working status.
     *
     * @param isWorking the status of Items to be fetched from a DB
     * @return {@link Long} count of records in database filtered by working status.
     */
    Long getCommonItemsFilteredByIsWorkingCount(boolean isWorking);

    /**
     * Assign {@link CommonItem} to Warehouse. Then Common Item can be used as {@link Item}
     *
     * @param commonItem
     * @return boolean value if {@link Item} was created
     */
    boolean assignCommonItemToWarehouse (CommonItem commonItem);

    /**
     * Creates and saves {@link ItemLog}.
     * Then forms message and send it to {@link MessageSender}.
     *
     * @param item the {@link CommonItem} to which some event is happened
     * @param dateTime current date and time
     * @param eventType {@link ItemEventType} of event
     * @param description message in the {@link ItemLog}
     */
}