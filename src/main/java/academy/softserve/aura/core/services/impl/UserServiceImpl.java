package academy.softserve.aura.core.services.impl;

import academy.softserve.aura.core.dao.DepartmentDao;
import academy.softserve.aura.core.dao.UserDao;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.exceptions.AuraPermissionsException;
import academy.softserve.aura.core.exceptions.AuraUserException;
import academy.softserve.aura.core.exceptions.AuraUserValidationException;
import academy.softserve.aura.core.exceptions.NoActiveAdminException;
import academy.softserve.aura.core.services.UserService;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static academy.softserve.aura.core.services.impl.Utils.*;
import static academy.softserve.aura.core.utils.Constants.DEPARTMENT_CHANGE_ERROR;

@Service
public class UserServiceImpl extends CrudServiceImpl<User> implements UserService {

    private Logger userLogger = LoggerFactory.getLogger("UserService logger");

    @Autowired
    private UserDao userDao;
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * INSERT action for single entity.
     *
     * @param entity the entity to be represented in DB
     * @return created and fetched from DB entity object
     * @throws {@link AuraUserValidationException}  in case of validation error
     *                and {@link AuraUserException} in case of user DB processing errors
     */
    @Transactional
    @Override
    public User create(User entity) {
        try {
            Long entityId = entity.getId();
            if (entityId == null || userDao.getElementByID(entityId) == null) {
//            create new
                if (entity.getLogin() == null) {
                    throw new AuraUserValidationException("Login must not be null");
                }
                if (entity.getPassword() == null) {
                    throw new AuraUserValidationException("Password must not be null");
                }
                if (entity.getUserRole() == null) {
                    throw new AuraUserValidationException("User role must not be null");
                }
                entity.setPassword(passwordEncoder.encode(entity.getPassword()));
                return userDao.addElement(entity);
            } else {
                return update(entity);
            }
        } catch (org.hibernate.exception.ConstraintViolationException e) {
            userLogger.info("Error adding user:" + e + " >>> \n >>> " + e.getCause().toString());
            throw new AuraUserValidationException("User validation error");
        } catch (HibernateException e) {
            userLogger.info("Error adding user:" + e + " >>> \n >>> " + e.getCause().toString());
            throw new AuraUserException("User processing error");
        }
    }

    @Override
    @Transactional
    public Collection<User> createAll(Collection<User> entities) {
//        hash passwords
        entities.stream().forEach(e -> e.setPassword(passwordEncoder.encode(e.getPassword())));
        return userDao.addAll(entities);
    }

    @Override
    @Transactional
    public User update(User entityForUpdate) {
        User entityFromDatabase = userDao.getElementByID(entityForUpdate.getId());
        if (entityFromDatabase == null) {
            return create(entityForUpdate);
        }

        Collection<SimpleGrantedAuthority> authorities = getUserWhoCalledMethodAuthorities();

        if (entityForUpdate.getLogin() != null && !entityForUpdate.getLogin().equals(entityFromDatabase.getLogin())) {
            checkIfUserCanChangeCredentials(authorities, entityForUpdate,entityFromDatabase);
        } else {
            entityForUpdate.setLogin(entityFromDatabase.getLogin());
        }

        if (entityForUpdate.getPassword() != null && !passwordEncoder.matches(entityForUpdate.getPassword(), entityFromDatabase.getPassword())) {
            checkIfUserCanChangeCredentials(authorities, entityForUpdate, entityFromDatabase);
        } else {
            entityForUpdate.setPassword(entityFromDatabase.getPassword());
        }

        if (entityForUpdate.getUserRole() == null) {
            entityForUpdate.setUserRole(entityFromDatabase.getUserRole());
        } else if (entityForUpdate.getUserRole() != null && !entityForUpdate.getUserRole().equals(entityFromDatabase.getUserRole())) {
            checkIfAdmin(authorities);
            if (entityForUpdate.getUserRole().equals(UserRole.MANAGER) && !entityFromDatabase.getUserRole().equals(UserRole.MANAGER)) {
                if (!managerPositionIsAvailable(entityForUpdate.getDepartment().getId())) {
                    userLogger.info("Error setting Manager: Department already have Manager");
                    throw new AuraUserException("This User cannot become Manager in current Department");
                }
            }
            if (entityFromDatabase.getUserRole() == UserRole.ADMIN && entityForUpdate.getUserRole() != UserRole.ADMIN) {
                if (!ifRoleChangingIsAvailable(entityFromDatabase.getId())) {
                    userLogger.info("Error changing role: it must be at least one active admin");
                    throw new AuraUserException("You can't change role for this fromDao");
                }
            }
        }

        if (entityForUpdate.isActive() != entityFromDatabase.isActive()) {
            checkIfAdmin(authorities);
            if (!entityForUpdate.isActive()) {
                checkIfExistsAtLeastOneAdmin(entityForUpdate);
            }
        }

        if (shouldUpdateDepartment(entityForUpdate.getDepartment(), entityFromDatabase.getDepartment())) {
            if (isUser()) {
                throw new AuraPermissionsException(DEPARTMENT_CHANGE_ERROR);
            }
            if (isManager()) {
                checkManagerRights(entityFromDatabase, entityForUpdate);
            }
        }

        return userDao.updateElement(entityForUpdate);
    }

    private void checkManagerRights(User entityFromDatabase, User entityForUpdate) {
        User loggedInUser = findByLogin(getUserLogin());
        Department loggedInUserDepartment = loggedInUser.getDepartment();

        if (entityFromDatabase.getUserRole().equals(UserRole.ADMIN)) {
            throw new AuraPermissionsException(DEPARTMENT_CHANGE_ERROR);
        }
        if (loggedInUserDepartment == null) {
            throw new AuraPermissionsException(DEPARTMENT_CHANGE_ERROR);
        }
        if (entityForUpdate.getDepartment() != null
                && !entityForUpdate.getDepartment().getId().equals(loggedInUserDepartment.getId())) {
            throw new AuraPermissionsException(DEPARTMENT_CHANGE_ERROR);
        }
        if (entityFromDatabase.getDepartment() != null
                && !entityFromDatabase.getDepartment().getId().equals(loggedInUserDepartment.getId())) {
            throw new AuraPermissionsException(DEPARTMENT_CHANGE_ERROR);
        }
    }

    // needed to avoid checks for NPE when using department1.equals(department2)
    private static boolean shouldUpdateDepartment(Department department1, Department department2) {
        if (department1 == department2) {
            return false;
        }
        if (department1 == null || department2 == null) {
            return true;
        }
        return !department1.getId().equals(department2.getId());
    }

    private void checkIfExistsAtLeastOneAdmin(User toUpdate) {
        if (!userDao.findByRoles(0, Integer.MAX_VALUE, Collections.singletonList(UserRole.ADMIN))
                .stream().filter(u -> !u.getId().equals(toUpdate.getId())).anyMatch(User::isActive)) {
            throw new NoActiveAdminException("Sorry but this user is the last active user");
        }
    }

    /**
     * check either person who called method can change isActive state
     */
    private static void checkIfAdmin(Collection<SimpleGrantedAuthority> authorities) {
        if (!authorities.stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {
            throw new AuraPermissionsException("Sorry, you are not allowed to do this");
        }
    }

    /**
     * check either person who called method can change credentials
     * for now it can be done only by admin or user himself
     *
     * @param authorities the roles of user who wants to update
     * @param entity User entity which comes from client
     * @param fromDao User entity which is taken from database and corresponds to the <code>entity</code>
     *
     * @throws {@link AuraPermissionsException}
     */
    private static void checkIfUserCanChangeCredentials(Collection<SimpleGrantedAuthority> authorities, User entity, User fromDao) {
        if(!(authorities.stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN")
                || entity.getId().equals(fromDao.getId())))) {
            throw new AuraPermissionsException("Sorry, you can't change password for this user");
        }
    }


    /**
     * Method checks simple rule: only 1 Manager for single Department.
     *
     * @param id the Id of the Department
     * @return true if Manager's position is available
     * @see User
     * @see UserRole
     * @see Department
     */
    private boolean managerPositionIsAvailable(Long id) {
        Collection<User> departmentUsers = departmentDao.getDepartmentUsers(id);
        return departmentUsers.stream().noneMatch(user -> user.getUserRole().equals(UserRole.MANAGER) && user.isActive());
    }

    /**
     * Method checks simple rule: at least 1 active Admin must left in system.
     *
     * @param id the Id of User
     * @return true if current User's role can be changed
     * @see User
     * @see UserRole
     */
    private boolean ifRoleChangingIsAvailable(Long id) {
        List<UserRole> userRoles = new ArrayList<>();
        userRoles.add(UserRole.ADMIN);
        Collection<User> allUsers = userDao.findByRoles(0,Integer.MAX_VALUE,userRoles);
        return allUsers.stream()
                .filter(user -> !user.getId().equals(id))
                .anyMatch(user -> user.getUserRole().equals(UserRole.ADMIN) && user.isActive());
    }

    @Override
    @Transactional
    public boolean delete(Long id) {
        User user = userDao.getElementByID(id);
        if (user != null && user.isActive()) {
            user.setActive(false);
            update(user);
            return true;
        } else {
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<User> findByRoles(int offset, int limit, List<UserRole> roles) {
        Collection<User> byRoles = userDao.findByRoles(offset, limit, roles);
//        use it because mapper will map department id (department in user is fetch lazy)
        byRoles.forEach(u -> u.getDepartment().getId());
        return byRoles;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<User> findByDepartments(int offset, int limit, List<Long> departmentIds) {
        Collection<User> byDepartments = userDao.findByDepartments(offset, limit, departmentIds);
        byDepartments.forEach(u -> u.getDepartment().getId());

        return byDepartments;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<User> findByDepartmentsAndRoles(int offset, int limit, List<Long> departmentIds, List<UserRole> roles) {
        Collection<User> byDepartmentsAndRoles = userDao.findByDepartmentsAndRoles(offset, limit, departmentIds, roles);
        byDepartmentsAndRoles.forEach(u -> u.getDepartment().getId());
        return byDepartmentsAndRoles;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<PrivateItem> getUserPrivateItems(Long id) {
        return userDao.getUserPrivateItems(id);
    }


    @Override
    @Transactional(readOnly = true)
    public Long getUsersCount() {
        return userDao.getUsersCount();
    }

    @Override
    @Transactional(readOnly = true)
    public Long getUsersFilteredByRolesCount(Collection<UserRole> roles) {
        return userDao.getUsersFilteredByRolesCount(roles);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getUsersFilteredByDepartmentsCount(Collection<Long> departmentIds) {
        return userDao.getUsersFilteredByDepartmentsCount(departmentIds);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getUsersFilteredByDepartmentAndRolesCount(Collection<Long> departmentIds, Collection<UserRole> roles) {
        return userDao.getUsersFilteredByDepartmentAndRolesCount(departmentIds, roles);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getUserPrivateItemsCount(Long userId) {
        return userDao.getUserPrivateItemsCount(userId);
    }


    @Override
    @Transactional
    public User findByLogin(String login) {
        return userDao.getByLogin(login);
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

}
