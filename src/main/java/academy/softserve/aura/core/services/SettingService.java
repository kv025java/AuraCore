package academy.softserve.aura.core.services;


import academy.softserve.aura.core.entity.Setting;

import java.util.Properties;

public interface SettingService extends CrudService<Setting>{

    Properties getSettings(Long id);
}
