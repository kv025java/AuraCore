package academy.softserve.aura.core.services.impl;

import academy.softserve.aura.core.dao.SettingDao;
import academy.softserve.aura.core.entity.Setting;
import academy.softserve.aura.core.exceptions.AuraException;
import academy.softserve.aura.core.services.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Properties;


@Service
public class SettingServiceImpl implements SettingService{

    private Setting set;

    @Autowired
    private SettingDao settingDao;

    private  SettingServiceImpl () {}

    public boolean buildSettings(Long id) {
        set = findById(id);
        return set != null;
    }

    public Properties getSettings(Long id) {
        if (buildSettings(id) && set.isActive()) {
            Properties settings = new Properties();

            settings.setProperty("bootstrap.servers", set.getKafkaHost().trim());
            List<String> topics = set.getKafkaTopicNames();

            if (topics != null && !topics.isEmpty()) {
                for (int i = 0; i < topics.size(); i++) {
                    String topicDefinition = "topic." + (i + 1);
                    settings.setProperty(topicDefinition, topics.get(i));
                }
            } else {
                throw new AuraException("Kafka properties error");
            }
            return settings;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public Setting create(Setting entity) {
        if (settingDao.getElementByID(entity.getId()) != null) {
            return settingDao.updateElement(entity);
        } else {
            return settingDao.addElement(entity);
        }
    }

    @Override
    @Transactional
    @Deprecated
    public Collection<Setting> createAll(Collection<Setting> entities) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public Setting findById(Long aLong) {
        return settingDao.getElementByID(aLong);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Setting> findAll() {
        return settingDao.getAllElements();
    }

    @Override
    @Transactional(readOnly = true)
    @Deprecated
    public Collection<Setting> findFew(int offset, int limit) {
        return null;
    }

    @Override
    @Transactional
    @Deprecated
    public Setting update(Setting entity) {
        return null;
    }

    @Override
    @Transactional
    public boolean delete(Long id) {
        Setting settings = settingDao.getElementByID(id);
        settings.setActive(false);
        settingDao.updateElement(settings);
        return true;
    }

    @Override
    @Deprecated
    public Long getRecordsCount() {
        return null;
    }
}
