package academy.softserve.aura.core.services.impl;

import academy.softserve.aura.core.dao.ItemDao;
import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.services.CommonItemService;
import academy.softserve.aura.core.services.ItemService;
import academy.softserve.aura.core.services.PrivateItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Collection;


@Service
public class ItemServiceImpl extends ItemSpecialServiceImpl<Item> implements ItemService {

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private PrivateItemService privateItemService;

    @Autowired
    private CommonItemService commonItemService;

    public ItemServiceImpl() {
        super(Item.class);
    }


    @Override
    @Transactional(readOnly = true)
    public Collection<Item> findFewByIsWorking(int offset, int limit, boolean isWorking) {
        return itemDao.findFewByIsWorking(offset, limit, isWorking);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getItemsCount() {
        return itemDao.getRecordsCount();
    }

    @Override
    @Transactional(readOnly = true)
    public Long getItemsFilteredByIsWorkingCount(boolean isWorking) {
        return itemDao.getItemsFilteredByIsWorkingCount(isWorking);
    }

    @Override
    @Transactional
    public boolean assignItemToUser(Item item, Long userId) {
        boolean isAddedToDB = itemDao.assignItemToUser(item, userId);

        if(isAddedToDB) {
            PrivateItem privateItem = privateItemService.findById(item.getId());
            logAndSendMessage(privateItem, OffsetDateTime.now(), ItemEventType.OWNER_CHANGED, "Item became Private. Owner ID: " + userId);

            privateItem.getComponents().forEach(item1 -> logAndSendMessage(item1, OffsetDateTime.now(), ItemEventType.OWNER_CHANGED, "Item became Private. Owner ID: " + userId));
        }

        return isAddedToDB;
    }

    @Override
    @Transactional
    public boolean assignItemToDepartment(Item item, Long departmentId) {
        boolean isAddedToDB = itemDao.assignItemToDepartment(item, departmentId);

        if(isAddedToDB) {
            CommonItem commonItem = commonItemService.findById(item.getId());
            logAndSendMessage(commonItem, OffsetDateTime.now(), ItemEventType.OWNER_CHANGED, "Item became Common. Owner ID: " + departmentId);

            commonItem.getComponents().forEach(item1 -> logAndSendMessage(item1, OffsetDateTime.now(), ItemEventType.OWNER_CHANGED, "Item became Common. Owner ID: " + departmentId));
        }

        return isAddedToDB;
    }

}
