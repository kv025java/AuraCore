package academy.softserve.aura.core.services;


import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;

import java.util.Collection;
import java.util.List;


/**
 * The interface to work with {@link User}.
 * It is extends basic {@link CrudService}.
 *
 * @see User
 */
public interface UserService extends CrudService<User> {

    /**
     * Gets few {@link User} from collection by their {@link UserRole}.
     *
     * @param offset the first entry to return from a collection
     * @param limit  the quantity of entries to return from a collection
     * @param roles  the {@link List} of {@link UserRole}
     * @return the collection of {@link User} which satisfy parameters
     */
    Collection<User> findByRoles(int offset, int limit, List<UserRole> roles);

    /**
     * Gets few {@link User} from collection by specific {@link Department} they are working in.
     *
     * @param offset the first entry to return from a collection
     * @param limit  the quantity of entries to return from a collection
     * @param departmentIds the {@link List} of {@link Department} ID
     * @return the collection of {@link User} who are working in specific {@link Department}
     */
    Collection<User> findByDepartments(int offset, int limit, List<Long> departmentIds);

    /**
     * Gets few {@link User} from collection by specific {@link Department} they are working in and their {@link UserRole}.
     *
     * @param offset the first entry to return from a collection
     * @param limit  the quantity of entries to return from a collection
     * @param departmentIds the {@link List} of {@link Department} ID
     * @param roles  the {@link List} of {@link UserRole}
     * @return the collection of {@link User} who are working in specific {@link Department} and have specific {@link UserRole}
     */
    Collection<User> findByDepartmentsAndRoles(int offset, int limit, List<Long> departmentIds, List<UserRole> roles);

    /**
     * Gets all {@link PrivateItem} from collection which are belong to specific {@link User}.
     *
     * @param id the id of {@link User}
     * @return the collection of {@link PrivateItem} which are belong to specific {@link User}
     */
    Collection<PrivateItem> getUserPrivateItems(Long id);


    /**
     * Gets number of {@link User} with definite {@link UserRole}s in database.
     *
     * @return {@link Long} count of {@link User} records.
     */
    Long getUsersCount();

    /**
     * Gets number of {@link User} with definite {@link UserRole}s in database.
     *
     * @param roles  the {@link List} of {@link UserRole}
     * @return {@link Long} count of {@link User} records with these roles.
     */
    Long getUsersFilteredByRolesCount(Collection<UserRole> roles);

    /**
     * Gets number of {@link User} with definite {@link Department} ids in database.
     *
     * @param departmentIds the {@link List} of {@link Department} ID
     * @return {@link Long} count of {@link User} records with these departments.
     */
    Long getUsersFilteredByDepartmentsCount(Collection<Long> departmentIds);

    /**
     * Gets number of {@link User} with definite {@link Department} ids and {@link UserRole}s in database.
     *
     * @param departmentIds the {@link List} of {@link Department} ID
     * @param roles  the {@link List} of {@link UserRole}
     * @return {@link Long} count of {@link User} records with these departments and user roles.
     */
    Long getUsersFilteredByDepartmentAndRolesCount(Collection<Long> departmentIds, Collection<UserRole> roles);

    /**
     * Gets number of {@link PrivateItem} that belong to {@link User}.
     *
     * @param userId the id of {@link User}
     * @return {@link Long} count of {@link PrivateItem}s that belong to {@link User}.
     */
    Long getUserPrivateItemsCount(Long userId);

    /**
     * Returns user by login
     *
     * @param login {@link String} value of login
     * @return {@link User} with this login or null if there is not any result
     */
    User findByLogin (String login);

}
