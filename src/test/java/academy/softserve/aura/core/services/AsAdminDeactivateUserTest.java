package academy.softserve.aura.core.services;

import academy.softserve.aura.core.dao.UserDao;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.exceptions.AuraPermissionsException;
import academy.softserve.aura.core.exceptions.NoActiveAdminException;
import academy.softserve.aura.core.services.impl.UserServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class AsAdminDeactivateUserTest {

    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserServiceImpl userService;

    private static List<User> usersInDao = new ArrayList<>();
    private static List<User> usersToBeUpdated = new ArrayList<>();
    private static ExecutorService executorService = Executors.newFixedThreadPool(4);
    private static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private Lock updatingMapLock = new ReentrantLock();

    @BeforeClass
    public static void setUpTest() {

        String pasasasa = passwordEncoder.encode("pasasasa");
        for (int i = 0; i < 100; i++) {
            User userInDaoArray = EntityFactory.createUserWithCredentialsAndNewDepartment();
            userInDaoArray.setUserRole(i % 2 == 0 ? UserRole.ADMIN : UserRole.MANAGER);
            userInDaoArray.setId(Long.valueOf(i));
            userInDaoArray.setActive(true);
            userInDaoArray.setPassword(pasasasa);
            usersInDao.add(userInDaoArray);

            User userInToBeUpdatedArray = EntityFactory.createUserWithCredentialsAndNewDepartment();
            userInToBeUpdatedArray.setUserRole(i % 2 == 0 ? UserRole.ADMIN : UserRole.MANAGER);
            userInToBeUpdatedArray.setId(Long.valueOf(i));
            userInToBeUpdatedArray.setActive(false);
            userInDaoArray.setPassword(pasasasa);
            usersToBeUpdated.add(userInToBeUpdatedArray);
        }


    }

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Utils.class);

//      to allow update isActive state
        PowerMockito.when(Utils.getUserWhoCalledMethodAuthorities()).thenReturn(new ArrayList<SimpleGrantedAuthority>() {{
            add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }});

        userService.setPasswordEncoder(passwordEncoder);


    }


    @Test(expected = AuraPermissionsException.class)
    public void deactivateUserWithNonAdminPermissions() {
        PowerMockito.when(Utils.getUserWhoCalledMethodAuthorities()).thenReturn(new ArrayList<SimpleGrantedAuthority>() {{
            add(new SimpleGrantedAuthority("ROLE_USER"));
            add(new SimpleGrantedAuthority("ROLE_MANAGER"));
        }});

        User toUpdate = EntityFactory.createUserWithCredentialsAndNewDepartment();
        toUpdate.setActive(false);

        User fromDao = EntityFactory.createUserWithCredentialsAndNewDepartment();
        fromDao.setId(toUpdate.getId());

        when(userDao.getElementByID(anyLong())).thenReturn(fromDao);
        userService.update(toUpdate);
    }

    @Test
    public void checkOneActiveAdminMustBeActiveAnyway() {
        for (int i = 0; i < 100; i++) {
            when(userDao.getElementByID(Long.valueOf(i))).thenReturn(usersInDao.get(i));
            when(userDao.updateElement(usersToBeUpdated.get(i))).thenReturn(usersToBeUpdated.get(i));
        }

        runUpdatingThread(0, 24);
        runUpdatingThread(25, 49);
        runUpdatingThread(50, 74);
        runUpdatingThread(75, 99);


        try {
            executorService.shutdown();
            executorService.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long count = usersInDao.stream().filter(u -> u.getUserRole().equals(UserRole.ADMIN) && u.isActive()).count();
        assertTrue(count >= 1);
    }

    private void runUpdatingThread(int start, int end) {
        executorService.submit(() -> {
            for (int i = start; i <= end; i++) {
                User forRollback = null;
                try {

                    forRollback = usersInDao.get(i);

                    callUpdate(i);


                } catch (NoActiveAdminException e) {
                    //rollback imitation
                    usersInDao.set(i, forRollback);

                    e.printStackTrace();
                    assertEquals(usersInDao.stream().filter(u -> u.getUserRole().equals(UserRole.ADMIN) && u.isActive()).count(), 1);
                }
            }
        });
    }

    private void callUpdate(int idIndexInList) {

        try {
            updatingMapLock.lock();
            //imitate that we updated element
            usersInDao.set(idIndexInList, usersToBeUpdated.get(idIndexInList));
            //list changed, we should return admins now
            when(userDao.findByRoles(anyInt(), anyInt(), anyCollection())).thenReturn(usersInDao.stream().filter(u -> u.getUserRole().equals(UserRole.ADMIN)).collect(Collectors.toList()));
            //call real method
        } finally {
            updatingMapLock.unlock();
        }

        userService.update(usersToBeUpdated.get(idIndexInList));
    }

}
