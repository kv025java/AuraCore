package academy.softserve.aura.core.services;

import academy.softserve.aura.core.dao.ItemLogDao;
import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.entity.ItemLog;
import academy.softserve.aura.core.services.impl.ItemLogServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class ItemLogServiceImplTest {

    @Mock
    private ItemLogDao itemLogDao;

    @InjectMocks
    private ItemLogServiceImpl itemLogService;

    private Item complex;
    private List<ItemLog> logList;
    private List<String> updatedItems;

    @Before
    public void setUp() throws Exception {
        complex = EntityFactory.createComplexItem();
        logList = new ArrayList<>();
        updatedItems = new ArrayList<>();
    }

    @Test
    public void createAndSaveSimpleItemLog() throws Exception {
        itemLogService.createAndSaveSimpleItemLog("1", EntityFactory.ITEM_USAGE_START_DATE, ItemEventType.OWNER_CHANGED, "test");
        Mockito.verify(itemLogDao, Mockito.times(1)).addElement(Mockito.any());
    }

    @Test
    public void getAllSinceTime() throws Exception {
        itemLogService.getAllSinceTime(EntityFactory.ITEM_USAGE_START_DATE);
        Mockito.verify(itemLogDao, Mockito.times(1)).getAllSinceTime(Mockito.any());
    }

    @Test
    public void getAllByItemId() throws Exception {
        itemLogService.getAllByItemId(1L);
        Mockito.verify(itemLogDao, Mockito.times(1)).getAllByItemId(Mockito.anyLong());
    }

    @Test
    public void getAllByItemIdAndTime() throws Exception {
        itemLogService.getAllByItemIdAndTime(1L, EntityFactory.ITEM_USAGE_START_DATE);
        Mockito.verify(itemLogDao, Mockito.times(1)).getAllByItemIdAndTime(Mockito.anyLong(), Mockito.any());
    }

    @Test
    public void generateCreatedLogsForHierarchy() throws Exception {
        itemLogService.generateCreatedLogsForHierarchy(complex, logList, updatedItems);
        Assert.assertTrue(logList.size() == complex.getComponents().size() + 1);
    }

    @Test
    public void generateUpgradedLogsForHierarchy() throws Exception {
        itemLogService.generateUpgradedLogsForHierarchy(complex, logList);
        Assert.assertTrue(logList.size() == complex.getComponents().size() + 1);
    }

    @Test
    public void generateAndSaveUpdateLogsForParent() throws Exception {
        Item item = EntityFactory.createNotComplexItem();
        item.setParentItem(complex.getComponents().get(0));
        complex.getComponents().get(0).setComponents(Arrays.asList(item));

        itemLogService.generateAndSaveUpdateLogsForParent(item, logList);
        Assert.assertTrue(logList.size() == 2);
    }

}