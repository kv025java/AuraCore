package academy.softserve.aura.core.services;

import academy.softserve.aura.core.dao.CrudDao;
import academy.softserve.aura.core.dao.UserDao;
import academy.softserve.aura.core.dao.impl.DepartmentDaoImpl;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.exceptions.AuraPermissionsException;
import academy.softserve.aura.core.services.impl.DepartmentServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static academy.softserve.aura.core.services.impl.Utils.getUserLogin;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class DepartmentServiceImplTest {

    @Mock
    private DepartmentDaoImpl departmentDao;

    @Mock
    private UserDao userDao;

    @Mock
    private CrudDao crudDao;

    @InjectMocks
    private DepartmentServiceImpl departmentService;

    private List<Department> entities = new ArrayList<>();
    private Department entity = EntityFactory.createEmptyDepartment();

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Utils.class);
    }

    @Test
    public void createExistingDepartment() {
        Mockito.when(departmentDao.getElementByID(entity.getId())).thenReturn(entity);
        departmentService.create(entity);
        Mockito.verify(departmentDao, Mockito.times(1)).updateElement(entity);
    }

    @Test
    public void createNotExistingDepartment() {
        Mockito.when(departmentDao.getElementByID(entity.getId())).thenReturn(null);
        departmentService.create(entity);
        Mockito.verify(departmentDao, Mockito.times(1)).addElement(entity);
    }

    @Test
    public void createAll() {
        departmentService.createAll(entities);
        Mockito.verify(crudDao, Mockito.times(1)).addAll(entities);
    }

    @Test
    public void findById() {
        departmentService.findById(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getElementByID(entity.getId());
    }

    @Test
    public void findByIdLoggedInAsManagerFromSameDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId());

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.findById(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getElementByID(entity.getId());
    }

    @Test(expected = AuraPermissionsException.class)
    public void findByIdLoggedInAsManagerFromDifferentDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId() / 2);

        Mockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.findById(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getElementByID(entity.getId());
    }

    @Test
    public void findAll() {
        departmentService.findAll();
        Mockito.verify(crudDao, Mockito.times(1)).getAllElements();
    }

    @Test
    public void findFew() {
        departmentService.findFew(1, 3);
        Mockito.verify(crudDao, Mockito.times(1)).getFew(1, 3);
    }

    @Test
    public void updateExistingDepartment() {
        PowerMockito.when(Utils.isManager()).thenReturn(false);
        Mockito.when(departmentDao.getElementByID(entity.getId())).thenReturn(entity);
        departmentService.update(entity);
        Mockito.verify(departmentDao, Mockito.times(1)).updateElement(entity);
    }

    @Test
    public void updateExistingDepartmentLoggedInAsManagerFromSameDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId());

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        Mockito.when(departmentDao.getElementByID(entity.getId())).thenReturn(entity);
        departmentService.update(entity);
        Mockito.verify(departmentDao, Mockito.times(1)).updateElement(entity);
    }

    @Test(expected = AuraPermissionsException.class)
    public void updateExistingDepartmentLoggedInAsManagerFromDifferentDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId() / 2);

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        Mockito.when(departmentDao.getElementByID(entity.getId())).thenReturn(entity);
        departmentService.update(entity);
    }

    @Test
    public void updateNotExistingDepartmentTest() {
        PowerMockito.when(Utils.isManager()).thenReturn(false);
        Mockito.when(departmentDao.getElementByID(entity.getId())).thenReturn(null);
        departmentService.update(entity);
        Mockito.verify(departmentDao, Mockito.times(1)).addElement(entity);
    }

    @Test
    public void updateNotExistingDepartmentTest_2() throws Exception {
        Mockito.when(departmentDao.getElementByID(entity.getId())).thenReturn(entity);
        entity.setId(null);
        departmentService.update(entity);
        Mockito.verify(departmentDao, Mockito.times(1)).addElement(entity);
    }

    @Test(expected = AuraPermissionsException.class)
    public void updateNotExistingDepartmentAsManagerWithNull() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(null);

        Department localEntity = EntityFactory.createEmptyDepartment();
        localEntity.setId(null);

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        Mockito.when(departmentDao.getElementByID(localEntity.getId())).thenReturn(null);
        departmentService.update(localEntity);
    }

    @Test
    public void delete() {
        departmentService.delete(entity.getId());
        Mockito.verify(crudDao, Mockito.times(1)).deleteElement(entity.getId());
    }

    @Test
    public void getDepartmentCommonItems() {
        PowerMockito.when(Utils.isManager()).thenReturn(false);
        departmentService.getDepartmentCommonItems(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getDepartmentCommonItems(entity.getId());
    }

    @Test
    public void getDepartmentCommonItemsAsManagerFromSameDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId());

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentCommonItems(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getDepartmentCommonItems(entity.getId());
    }

    @Test(expected = AuraPermissionsException.class)
    public void getDepartmentCommonItemsAsManagerFromDifferentSameDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId() / 2);

        Mockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentCommonItems(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getDepartmentCommonItems(entity.getId());
    }

    @Test(expected = AuraPermissionsException.class)
    public void getDepartmentCommonItemsAsManagerWithNull() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(null);

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentCommonItems(entity.getId());
    }

    @Test
    public void getDepartmentUsersTest() {
        PowerMockito.when(Utils.isManager()).thenReturn(false);
        departmentService.getDepartmentUsers(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getDepartmentUsers(entity.getId());
    }

    @Test
    public void getDepartmentUsersAsManagerFromSameDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId());

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentUsers(entity.getId());
    }

    @Test(expected = AuraPermissionsException.class)
    public void getDepartmentUsersAsManagerFromDifferentDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId() / 2);

        Mockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentUsers(entity.getId());
    }

    @Test(expected = AuraPermissionsException.class)
    public void getDepartmentUsersAsManagerWithNull() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(null);

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentUsers(entity.getId());
    }

    @Test
    public void getDepartmentCount() {
        PowerMockito.when(Utils.isManager()).thenReturn(false);
        departmentService.getDepartmentCount();
        Mockito.verify(departmentDao, Mockito.times(1)).getRecordsCount();
    }

    @Test
    public void getDepartmentCommonItemsCount() {
        departmentService.getDepartmentCommonItemsCount(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1))
                .getDepartmentCommonItemsCount(entity.getId());
    }

    @Test
    public void getDepartmentCommonItemsCountAsManagerFromSameDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId());

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentCommonItemsCount(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1))
                .getDepartmentCommonItemsCount(entity.getId());
    }

    @Test(expected = AuraPermissionsException.class)
    public void getDepartmentCommonItemsCountAsManagerFromDifferentDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId() / 2);

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentCommonItemsCount(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1))
                .getDepartmentCommonItemsCount(entity.getId());
    }

    @Test(expected = AuraPermissionsException.class)
    public void getDepartmentCommonItemsCountAsManagerWithNull() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(null);

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentCommonItemsCount(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1))
                .getDepartmentCommonItemsCount(entity.getId());
    }

    @Test
    public void getDepartmentUsersCount() {
        departmentService.getDepartmentUsersCount(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getDepartmentUsersCount(entity.getId());
    }

    @Test
    public void getDepartmentUsersCountAsManagerFromSameDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId());

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentUsersCount(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getDepartmentUsersCount(entity.getId());
    }

    @Test(expected = AuraPermissionsException.class)
    public void getDepartmentUsersCountAsManagerFromDifferentDepartment() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(entity.getId() / 2);

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentUsersCount(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getDepartmentUsersCount(entity.getId());
    }

    @Test(expected = AuraPermissionsException.class)
    public void getDepartmentUsersCountAsManagerWithNull() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        user.getDepartment().setId(null);

        PowerMockito.when(Utils.isManager()).thenReturn(true);
        Mockito.when(userDao.getByLogin(getUserLogin())).thenReturn(user);
        departmentService.getDepartmentUsersCount(entity.getId());
        Mockito.verify(departmentDao, Mockito.times(1)).getDepartmentUsersCount(entity.getId());
    }

}
