package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.swagger.dto.DtoEntitiesFactory;
import academy.softserve.aura.core.swagger.model.DepartmentDto;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class DepartmentMapperTest {
    @Autowired
    DtoMapper<DepartmentDto, Department> mapper;

    @Test
    public void toDto() throws Exception {
        Department department = EntityFactory.createEmptyDepartment();
        DepartmentDto dto = mapper.toDto(department);
        assertEquals(department.getId(), dto.getId());
        assertEquals(department.getName(), dto.getName());
        assertNotNull(dto.getContacts());
        assertNull(mapper.toDto(null));

    }

    @Test
    public void toEntity() throws Exception {
        DepartmentDto dto = DtoEntitiesFactory.createEmptyDepartment();
        Department department = mapper.toEntity(dto);
        assertEquals(department.getId(), dto.getId());
        assertEquals(department.getName(), dto.getName());
        assertNotNull(department.getContacts());
        assertNull(mapper.toEntity(null));

    }

}