package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Setting;
import academy.softserve.aura.core.swagger.model.SettingDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class SettingMapperTest {

    @Autowired
    DtoMapper<SettingDto, Setting> mapper;
    private Setting setting = new Setting();
    private SettingDto settingDto = new SettingDto();

    @Before
    public void makeStubs() {
        List<String> list1 = new ArrayList<>();
        list1.add("TopicName1");
        list1.add("TopicName2");

        List<String> list2 = new ArrayList<>();

        setting.setId(1L);
        setting.setActive(true);
        setting.setMicroserviceHost("123");
        setting.setKafkaHost("345");
        setting.setKafkaTopicNames(list1);
        settingDto.setKafkaTopicNames(list2);
        settingDto.setKafkaHost("456");
        settingDto.setMicroserviceHost("321");
        settingDto.setIsActive(false);
        settingDto.setId(1L);
    }

    @Test
    public void toDto() throws Exception{
        settingDto = mapper.toDto(setting);
        assertEquals(setting.getId(), settingDto.getId());
        assertEquals(setting.isActive(), settingDto.getIsActive());
        assertEquals(setting.getKafkaHost(), settingDto.getKafkaHost());
        assertEquals(setting.getKafkaTopicNames(), settingDto.getKafkaTopicNames());
        assertEquals(setting.getMicroserviceHost(), settingDto.getMicroserviceHost());

        assertNull(mapper.toDto(null));
    }

    @Test
    public void toEntity() throws Exception{
        setting = mapper.toEntity(settingDto);
        assertEquals(setting.getId(), settingDto.getId());
        assertEquals(setting.isActive(), settingDto.getIsActive());
        assertEquals(setting.getMicroserviceHost(), settingDto.getMicroserviceHost());
        assertEquals(setting.getKafkaTopicNames(), settingDto.getKafkaTopicNames());
        assertEquals(setting.getKafkaHost(), settingDto.getKafkaHost());

        assertNull(mapper.toEntity(null));
    }
}
