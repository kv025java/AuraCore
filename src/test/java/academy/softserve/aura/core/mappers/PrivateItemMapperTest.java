package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.swagger.dto.DtoEntitiesFactory;
import academy.softserve.aura.core.swagger.model.PrivateItemDto;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class PrivateItemMapperTest {
    @Autowired
    PrivateItemMapper mapper;

    @Test
    public void toDto() {
        PrivateItem item = EntityFactory.createComplexPrivateItem();
        PrivateItemDto mappedDto = mapper.toDto(item);
        assertTrue(mappedDto.getIsWorking());
        assertEquals(mappedDto.getId(), item.getId());
        assertEquals(mappedDto.getUserId(), item.getOwner().getId());
        assertEquals(mappedDto.getManufacturer(), item.getManufacturer());
        assertEquals(mappedDto.getModel(), item.getModel());
        assertEquals(mappedDto.getStartPrice(), item.getStartPrice());
        assertEquals(mappedDto.getCurrentPrice(), item.getCurrentPrice());
        assertTrue(mappedDto.getNominalResource() == item.getNominalResource());
        assertEquals(mappedDto.getDescription(), item.getDescription());
        assertNotNull(mappedDto.getComponents());
        assertEquals(mappedDto.getItemType(), item.getClass().getSimpleName());
        assertEquals(mappedDto.getUsageStartDate(), item.getUsageStartDate());
        assertEquals(mappedDto.getManufactureDate(), item.getManufactureDate());
        assertTrue(mappedDto.getAgingFactor() == item.getAgingFactor());

        item.setComponents(null);
        assertNotNull(mapper.toDto(item).getComponents());

        item.setComponents(new ArrayList<>());
        assertNotNull(mapper.toDto(item).getComponents());
        assertTrue(mapper.toDto(item).getComponents().isEmpty());

        item.getOwner().setId(null);
        assertNull(mapper.toDto(item).getUserId());
        item.setOwner(null);
        assertNull(mapper.toDto(item).getUserId());

        item = null;
        assertNull(mapper.toDto((item)));
    }

    @Test
    public void toEntity() {
        PrivateItemDto dto = DtoEntitiesFactory.createComplexPrivateItemDto();
        PrivateItem mappedItem = mapper.toEntity(dto);
        assertTrue(mappedItem.isWorking());
        assertEquals(mappedItem.getId(), dto.getId());
        assertEquals(dto.getUserId(), mappedItem.getOwner().getId());
        assertEquals(mappedItem.getManufacturer(), dto.getManufacturer());
        assertEquals(mappedItem.getModel(), dto.getModel());
        assertEquals(mappedItem.getStartPrice(), dto.getStartPrice());
        assertEquals(mappedItem.getCurrentPrice(), dto.getCurrentPrice());
        assertTrue(mappedItem.getNominalResource() == dto.getNominalResource());
        assertEquals(mappedItem.getDescription(), dto.getDescription());
        assertNotNull(mappedItem.getComponents());
        assertEquals(dto.getItemType(), mappedItem.getClass().getSimpleName());
        assertEquals(mappedItem.getUsageStartDate(), dto.getUsageStartDate());
        assertEquals(mappedItem.getManufactureDate(), dto.getManufactureDate());
        assertTrue(mappedItem.getAgingFactor() == dto.getAgingFactor());
        assertNotNull(mapper.toEntity(dto).getComponents().get(0).getParentItem());

        dto.setComponents(null);
        assertNotNull(mapper.toEntity(dto).getComponents());
        assertNull(mapper.toEntity(dto).getParentItem());

        dto.setComponents(new ArrayList<>());
        assertNotNull(mapper.toEntity(dto).getComponents());
        assertTrue(mapper.toEntity(dto).getComponents().isEmpty());

        dto.setIsWorking(null);
        assertFalse(mapper.toEntity(dto).isWorking());

        dto.setNominalResource(null);
        assertTrue(mapper.toEntity(dto).getNominalResource() == 0.0);


        dto = null;
        assertNull(mapper.toEntity((dto)));
    }

}