package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.entity.ItemLog;
import academy.softserve.aura.core.exceptions.AuraMapperIllegalMethodException;
import academy.softserve.aura.core.swagger.model.ItemLogDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class ItemLogMapperTest {
    @Autowired
    private DtoMapper<ItemLogDto, ItemLog> mapper;

    @Test
    public void toDto() throws Exception {
        ItemLog itemLog = new ItemLog();
        Item item = new Item();

        item.setId(2L);
        itemLog.setItemId(item.getId().toString());
        itemLog.setEventDate(java.time.OffsetDateTime.now());
        itemLog.setId(1L);
        itemLog.setDescription("test");
        itemLog.setEventType(ItemEventType.ITEM_CREATED);

        ItemLogDto dto = mapper.toDto(itemLog);

        assertEquals(itemLog.getId(), dto.getId());
        assertEquals(itemLog.getItemId(), dto.getItemId());
        assertEquals(itemLog.getEventType().toString(), dto.getItemEventType());
        assertEquals(itemLog.getDescription(), dto.getDescription());
        assertEquals(itemLog.getEventDate(), dto.getEventDate());
        assertNull(mapper.toDto(null));

//        itemLog.setId(null);
//        item.setId(null);
//        itemLog.setEventType(null);
//        //itemLog.setItemId(item.getId().toString());
//        dto = mapper.toDto(itemLog);
//        assertNull(dto.getId());
//        assertNull(dto.getItemEventType());
//        assertNull(dto.getItemId());
//        item.setId(2L);
//        itemLog.setItemId(null);
//        dto = mapper.toDto(itemLog);
//        assertNull(dto.getItemId());
    }

    @Test(expected = AuraMapperIllegalMethodException.class)
    public void toEntity() throws Exception {
        mapper.toEntity(new ItemLogDto());
    }

}