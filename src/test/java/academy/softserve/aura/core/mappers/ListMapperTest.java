package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Email;
import academy.softserve.aura.core.swagger.model.EmailDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class ListMapperTest {
    private ListMapper<EmailDto, Email> listMapper = new ListMapper<EmailDto, Email>(new EmailMapper());

    @Test
    public void toDtoList() throws Exception {
        assertNotNull(listMapper.toDtoList(null));
        List<Email> emailList = new ArrayList<>();
        assertNotNull(listMapper.toDtoList(emailList));


    }

    @Test
    public void toEntityList() throws Exception {
        assertNotNull(listMapper.toEntityList(null));
        List<EmailDto> emailDtoList = new ArrayList<>();
        assertNotNull(listMapper.toEntityList(emailDtoList));
    }

}