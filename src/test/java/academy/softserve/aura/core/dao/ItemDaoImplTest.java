package academy.softserve.aura.core.dao;

import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:daos/test_dao_application_context.xml")
public class ItemDaoImplTest {

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private DepartmentDao departmentDao;

    private Item item1;
    private Item item2;
    private Item item3;
    private User user;
    private Department department;

    @Before
    public void setUp() {
        item1 = EntityFactory.createNotComplexItem();
        item2 = EntityFactory.createNotComplexItem();
        item3 = EntityFactory.createNotComplexItem();
        user = EntityFactory.createUserWithCredentialsWithotDepartment();
        department = EntityFactory.createEmptyDepartment();

        item1.setWorking(true);
        item1.setWorking(true);
        item1.setWorking(false);

        itemDao.addElement(item1);
        itemDao.addElement(item2);
        itemDao.addElement(item3);
        userDao.addElement(user);
        departmentDao.addElement(department);
    }

    @Test
    @Transactional
    public void findFewByIsWorking() throws Exception {
        Collection<Item> workingItems = itemDao.findFewByIsWorking(0, 10, true);
        Assert.assertFalse(workingItems.isEmpty());
        Assert.assertEquals(2, workingItems.size());
    }

    @Test
    @Transactional
    public void assignItemToUser() throws Exception {
        boolean result = itemDao.assignItemToUser(item1, user.getId());
        Assert.assertTrue(result);
    }

    @Test
    @Transactional
    public void assignItemToDepartment() throws Exception {
        boolean result = itemDao.assignItemToDepartment(item1, department.getId());
        Assert.assertTrue(result);
    }

    @Test
    @Transactional
    public void getItemsFilteredByIsWorkingCount() throws Exception {
        long count = itemDao.getItemsFilteredByIsWorkingCount(true);
        assertEquals(2L, count);
    }

    @Test
    @Transactional
    public void getRecordsCount() throws Exception {
        long count = itemDao.getRecordsCount();
        assertEquals(3L, count);
    }
}
