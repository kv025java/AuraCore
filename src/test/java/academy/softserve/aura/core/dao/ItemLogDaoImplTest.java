package academy.softserve.aura.core.dao;


import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.entity.ItemLog;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:daos/test_dao_application_context.xml")
public class ItemLogDaoImplTest {

    @Autowired
    private ItemLogDao itemLogDao;

    private ItemLog itemLog1;
    private ItemLog itemLog2;
    private ItemLog itemLog3;
    private ItemLog testLog;
    private List<ItemLog> list;

    @Before
    public void setUp() throws Exception {
        Item item1 = EntityFactory.createNotComplexItem();
        Item item2 = EntityFactory.createNotComplexItem();
        Item item3 = EntityFactory.createNotComplexItem();
        Item item4 = EntityFactory.createNotComplexItem();

        itemLog1 = new ItemLog(item1.getId().toString(), OffsetDateTime.now(), ItemEventType.ITEM_CREATED, "description");
        itemLog2 = new ItemLog(item2.getId().toString(), OffsetDateTime.now(), ItemEventType.ITEM_CREATED, "description");
        itemLog3 = new ItemLog(item3.getId().toString(), OffsetDateTime.now(), ItemEventType.ITEM_REPAIRED, "description");
        testLog = new ItemLog(item4.getId().toString(), OffsetDateTime.now(), ItemEventType.ITEM_UPDATED, "description");

        list = new ArrayList<>();
        list.add(itemLog2);
        list.add(itemLog3);

        itemLogDao.addElement(itemLog1);
        itemLogDao.addAll(list);
    }


    @Test
    @Transactional
    public void getAllTest() throws Exception {
        assertEquals(list.size()+1, itemLogDao.getAllElements().size());
    }

    @Test
    @Transactional
    public void addElementTest() throws Exception {
        itemLogDao.addElement(testLog);
        assertEquals(list.size() + 2, itemLogDao.getAllElements().size());
    }

    @Test
    @Transactional
    public void addCollectionTest() throws Exception {
        List<ItemLog> testList = new ArrayList<>();
        testList.add(testLog);
        itemLogDao.addAll(testList);
        assertEquals(list.size() + 2, itemLogDao.getAllElements().size());
    }

    @Test
    @Transactional
    public void getAllSinceTimeTest() throws Exception {
        OffsetDateTime time = EntityFactory.FUTURE_DATE;
        Collection<ItemLog> result = itemLogDao.getAllSinceTime(time);
        assertEquals(0, result.size());

        time = EntityFactory.ITEM_USAGE_START_DATE;
        result = itemLogDao.getAllSinceTime(time);
        assertEquals(3, result.size());
    }

    @Test
    @Transactional
    public void getAllByItemIdTest() throws Exception {
        Long itemId = Long.parseLong(itemLog1.getItemId());
        Collection<ItemLog> result = itemLogDao.getAllByItemId(itemId);
        assertEquals(1, result.size());
        assertTrue(result.contains(itemLog1));
    }

    @Test
    @Transactional
    public void getAllByItemIdAndTimeTest() throws Exception {
        Long itemId = Long.parseLong(itemLog1.getItemId());
        OffsetDateTime now = EntityFactory.FUTURE_DATE;

        Collection<ItemLog> result = itemLogDao.getAllByItemIdAndTime(itemId, now);
        assertEquals(0, result.size());

        now = EntityFactory.ITEM_USAGE_START_DATE;
        result = itemLogDao.getAllByItemIdAndTime(itemId, now);
        assertEquals(1, result.size());
    }
}
