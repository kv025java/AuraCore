package academy.softserve.aura.core.dao;

import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:daos/test_dao_application_context.xml")
public class PrivateItemDaoImplTest {

    @Autowired
    UserDao userDao;

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private PrivateItemDao privateItemDao;

    private List<Long> privateItemIds = null;

    private List<Long> departmentIds = null;
    private User user1;
    private User user2;
    private User user3;

    @Before
    public void setUp() throws Exception {
        Department department1 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department1);
        Department department2 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department2);
        Department department3 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department3);

        user1 = EntityFactory.createUserWithCredentialsWithotDepartment();
        user1.setDepartment(department1);
        user1.setUserRole(UserRole.USER);
        user1.setFirstName("User");
        user2 = EntityFactory.createUserWithCredentialsWithotDepartment();
        user2.setUserRole(UserRole.MANAGER);
        user2.setDepartment(department2);
        user2.setFirstName("Manager");
        user3 = EntityFactory.createUserWithCredentialsWithotDepartment();
        user3.setUserRole(UserRole.ADMIN);
        user3.setDepartment(department3);
        user3.setFirstName("Admin");

        PrivateItem privateItem1 = EntityFactory.createNotComplexPrivateItem();
        privateItem1.setModel("item1");
        PrivateItem privateItem2 = EntityFactory.createNotComplexPrivateItem();
        privateItem2.setModel("item2");
        PrivateItem privateItem3 = EntityFactory.createNotComplexPrivateItem();
        privateItem3.setModel("item3");

        privateItem1.setOwner(user1);
        privateItem2.setOwner(user2);
        privateItem3.setOwner(user3);

        privateItem1.setWorking(true);
        privateItem2.setWorking(true);
        privateItem3.setWorking(false);

        userDao.addElement(user1);
        userDao.addElement(user2);
        userDao.addElement(user3);

        privateItemDao.addElement(privateItem1);
        privateItemDao.addElement(privateItem2);
        privateItemDao.addElement(privateItem3);

        privateItemIds = new ArrayList<>();
        privateItemIds.add(privateItem1.getId());
        privateItemIds.add(privateItem2.getId());
        privateItemIds.add(privateItem3.getId());

        departmentIds = new ArrayList<>();
        departmentIds.add(department1.getId());
        departmentIds.add(department2.getId());
    }

    @Test
    @Transactional
    public void findFewByIsWorking() throws Exception {
        List<PrivateItem> privateItems = (List<PrivateItem>) privateItemDao.findFewByIsWorking(0,2,true);
        assertEquals(2,privateItems.size());
        assertEquals("item1",privateItems.get(0).getModel());
        assertEquals("item2",privateItems.get(1).getModel());

    }

    @Test
    @Transactional
    public void getPrivateItemUsers() throws Exception {
        //from logs
    }

    @Test
    @Transactional
    public void assignPrivateItemToWarehouse() throws Exception {
        List<PrivateItem> allElements = (List) privateItemDao.getAllElements();
        Assert.assertFalse(allElements.isEmpty());
        boolean result = privateItemDao.assignPrivateItemToWarehouse(allElements.get(0));
        Assert.assertTrue(result);
    }

    @Test
    @Transactional
    public void getRecordsCount() {
        long count = privateItemDao.getRecordsCount();
        assertEquals(3L, count);
    }

    @Test
    @Transactional
    public void getPrivateItemsFilteredByIsWorkingCount() {
        long count = privateItemDao.getPrivateItemsFilteredByIsWorkingCount(true);
        assertEquals(2L, count);
    }

}
