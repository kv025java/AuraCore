package academy.softserve.aura.core.dao;

import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:daos/test_dao_application_context.xml")
public class UserDaoImplTest {

    @Autowired
    UserDao userDao;

    @Autowired
    DepartmentDao departmentDao;

    @Autowired
    PrivateItemDao privateItemDao;

    private List<UserRole> roles = null;
    private List<Long> departmentIds = null;
    private List<User> users = new ArrayList<>();
    private User user1;
    private User user2;
    private User user3;

    @Before
    public void setUp() throws Exception {
        Department department1 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department1);
        Department department2 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department2);
        Department department3 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department3);

        user1 = EntityFactory.createUserWithCredentialsWithotDepartment();
        user1.setDepartment(department1);
        user1.setUserRole(UserRole.USER);
        user1.setFirstName("User");
        user2 = EntityFactory.createUserWithCredentialsWithotDepartment();
        user2.setUserRole(UserRole.MANAGER);
        user2.setDepartment(department2);
        user2.setFirstName("Manager");
        user3 = EntityFactory.createUserWithCredentialsWithotDepartment();
        user3.setUserRole(UserRole.ADMIN);
        user3.setDepartment(department3);
        user3.setFirstName("Admin");

        PrivateItem privateItem1 = EntityFactory.createNotComplexPrivateItem();
        PrivateItem privateItem2 = EntityFactory.createNotComplexPrivateItem();
        PrivateItem privateItem3 = EntityFactory.createNotComplexPrivateItem();

        privateItem1.setOwner(user1);
        privateItem2.setOwner(user2);
        privateItem3.setOwner(user3);

        users.add(user1);
        users.add(user2);
        users.add(user3);

        userDao.addElement(user1);
        userDao.addElement(user2);
        userDao.addElement(user3);

        privateItemDao.addElement(privateItem1);
        privateItemDao.addElement(privateItem2);
        privateItemDao.addElement(privateItem3);

        roles = new ArrayList<>();
        roles.add(UserRole.MANAGER);
        roles.add(UserRole.ADMIN);

        departmentIds = new ArrayList<>();
        departmentIds.add(department1.getId());
        departmentIds.add(department2.getId());
    }

    @Test
    @Transactional
    public void findByRoles() throws Exception {
        List<User> users = (List<User>) userDao.findByRoles(0,2,roles);
        assertEquals("Manager",users.get(0).getFirstName());
        assertEquals("Admin",users.get(1).getFirstName());
        users.stream().forEach(user ->  assertNotEquals(UserRole.USER,user.getUserRole()));
    }

    @Test
    @Transactional
    public void findByDepartments() throws Exception {
        List<User> users = (List<User>) userDao.findByDepartments(0,2,departmentIds);
        assertEquals("User",users.get(0).getFirstName());
        assertEquals("Manager",users.get(1).getFirstName());
    }

    @Test
    @Transactional
    public void findByDepartmentsAndRoles() throws Exception {
        List<User> users = (List<User>) userDao.findByDepartmentsAndRoles(0,2,departmentIds,roles);
        users.stream().forEach(user -> System.out.println(user.getFirstName()));
        assertEquals("Manager",users.get(0).getFirstName());
    }

    @Test
    @Transactional
    public void getUserPrivateItems() throws Exception {
        List<PrivateItem> privateItems = (List<PrivateItem>) userDao.getUserPrivateItems(user1.getId());
        assertNotNull(privateItems);
        assertEquals(1, privateItems.size());
    }

    @Test
    @Transactional
    public void addElement() throws Exception {
        User user = EntityFactory.createUserWithCredentialsWithotDepartment();
        user.setFirstName("Test name");
        Department department1 = EntityFactory.createEmptyDepartment();
        departmentDao.addElement(department1);
        user.setDepartment(department1);
        userDao.addElement(user);

        User testUser = userDao.getElementByID(user.getId());
        assertEquals("Test name",testUser.getFirstName());
    }

    @Test
    @Transactional
    public void addAll() throws Exception {
        List<User> testUsers = (ArrayList<User>) userDao.getAllElements();
        assertNotNull(testUsers);
        assertEquals(users.size(), testUsers.size());
    }

    @Test
    @Transactional
    public void updateElement() throws Exception {
        user1.setFirstName("New firstname");
        User user = userDao.updateElement(user1);
        assertNotNull(user);
        assertEquals(user1.getFirstName(), user.getFirstName());
    }

    @Test
    @Transactional
    public void getElementByID() throws Exception {
        User user = userDao.getElementByID(user1.getId());
        assertNotNull(user);
        assertEquals(user1.getFirstName(), user.getFirstName());
    }

    @Test
    @Transactional
    public void getAllElements() throws Exception {
        List<User> testUsers = (ArrayList<User>) userDao.getAllElements();
        assertNotNull(testUsers);
        assertEquals(testUsers.get(0).getFirstName(), user1.getFirstName());
        assertEquals(testUsers.get(1).getFirstName(), user2.getFirstName());
        assertEquals(testUsers.get(2).getFirstName(), user3.getFirstName());
    }

    @Test
    @Transactional
    public void deleteElement() throws Exception {
        User user = EntityFactory.createUserWithCredentialsWithotDepartment();
        userDao.addElement(user);
        assertTrue(userDao.getAllElements().size() == users.size()+1);
        assertTrue(userDao.deleteElement(user.getId()));
        assertTrue(userDao.getAllElements().size() == users.size());
    }

    @Test
    @Transactional
    public void getFew() throws Exception {
        List<User> testUsers = (ArrayList<User>) userDao.getFew(1, 3);
        assertNotNull(testUsers);
        assertTrue(testUsers.size() == 2);
    }

    @Test
    @Transactional
    public void getUsersFilteredByRolesCount() {
        List<UserRole> userRoles =  new ArrayList<>();
        userRoles.add(UserRole.USER);

        long count = userDao.getUsersFilteredByRolesCount(userRoles);
        assertEquals(1L, count);
    }

    @Test
    @Transactional
    public void getUsersFilteredByDepartmentsCount() {
        List<Long> deptIds = new ArrayList<>();
        deptIds.add(departmentIds.get(0));

        long count = userDao.getUsersFilteredByDepartmentsCount(deptIds);
        assertEquals(1L, count);
    }

    @Test
    @Transactional
    public void getUsersFilteredByDepartmentAndRolesCount() {
        List<UserRole> userRoles =  new ArrayList<>();
        List<Long> deptIds = new ArrayList<>();

        userRoles.add(UserRole.USER);
        deptIds.add(departmentIds.get(0));

        long count = userDao.getUsersFilteredByDepartmentAndRolesCount(deptIds, userRoles);
        assertEquals(1L, count);
    }

    @Test
    @Transactional
    public void getUserPrivateItemsCount() {
        long count = userDao.getUserPrivateItemsCount(user1.getId());
        assertEquals(1L, count);
    }
}
