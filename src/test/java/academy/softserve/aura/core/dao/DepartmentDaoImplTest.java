package academy.softserve.aura.core.dao;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:daos/test_dao_application_context.xml")
public class DepartmentDaoImplTest {
    private Long idForFind;
    private int tableCount;
    private Department departmentForUpdate;
    private List<Long> addedDepartmentIDs = new ArrayList<>();

    @Autowired
    private UserDao userDao;

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private CommonItemDao commonItemDao;

    @Before
    public void setUp() throws Exception {
        Department department1 = EntityFactory.createEmptyDepartment();
        department1.setName("testElement");
        Department department2 = EntityFactory.createEmptyDepartment();
        Department department3 = EntityFactory.createEmptyDepartment();
        List<Department> departments = new ArrayList<>();
        departments.add(department1);
        departments.add(department2);
        departments.add(department3);
        departmentDao.addAll(departments);

        List<Department> testDepartments = (List<Department>) departmentDao.getAllElements();
        for (Department department : testDepartments) {
            addedDepartmentIDs.add(department.getId());
        }

        idForFind = testDepartments.get(0).getId();
        tableCount = testDepartments.size();
        departmentForUpdate = department1;

        User user1 = EntityFactory.createUserWithCredentialsWithotDepartment();
        user1.setDepartment(departmentForUpdate);
        user1.setUserRole(UserRole.USER);
        user1.setFirstName("User");
        User user2 = EntityFactory.createUserWithCredentialsWithotDepartment();
        user2.setUserRole(UserRole.MANAGER);
        user2.setDepartment(departmentForUpdate);
        user2.setFirstName("Manager");
        User user3 = EntityFactory.createUserWithCredentialsWithotDepartment();
        user3.setUserRole(UserRole.ADMIN);
        user3.setDepartment(department3);
        user3.setFirstName("Admin");

        userDao.addElement(user1);
        userDao.addElement(user2);
        userDao.addElement(user3);

        CommonItem commonItem1 = EntityFactory.createNotComplexCommonItem();
        commonItem1.setDepartment(department1);
        commonItemDao.addElement(commonItem1);
    }

    @Test
    @Transactional
    public void getDepartmentCommonItems() throws Exception {
        List<CommonItem> items = (ArrayList<CommonItem>) departmentDao.getDepartmentCommonItems(idForFind);
        assertNotNull(items);
        assertEquals(1, items.size());
    }

    @Test
    @Transactional
    public void getDepartmentUsers() throws Exception {
        List<User> users = (List<User>) departmentDao.getDepartmentUsers(departmentForUpdate.getId());
        users.stream().forEach(user -> assertNotEquals("User", user.getUserRole()));
    }

    @Test
    @Transactional
    public void addElement() throws Exception {
        Department department = EntityFactory.createEmptyDepartment();
        department.setName("testElement");
        departmentDao.addElement(department);
        List<Department> departments = (List<Department>) departmentDao.getAllElements();
        Assert.assertEquals(department.getName(), departments.get(0).getName());
    }

    @Test
    @Transactional
    public void addAll() throws Exception {
        Department department1 = EntityFactory.createEmptyDepartment();
        Department department2 = EntityFactory.createEmptyDepartment();
        List<Department> departments = new ArrayList<>();
        departments.add(department1);
        departments.add(department2);
        departmentDao.addAll(departments);
        List<Department> receivedDepartments = (List<Department>) departmentDao.getAllElements();
        Assert.assertEquals(departments.size() + tableCount, receivedDepartments.size());
    }

    @Test
    @Transactional
    public void updateElement() throws Exception {
        departmentForUpdate.setName("Updated department");
        departmentForUpdate.setId(idForFind);
        departmentDao.updateElement(departmentForUpdate);
        Department updatedDepartment = departmentDao.getElementByID(idForFind);
        Assert.assertEquals("Updated department", updatedDepartment.getName());
    }

    @Test
    @Transactional
    public void getElementByID() throws Exception {
        Department department = departmentDao.getElementByID(idForFind);
        assertEquals("testElement", department.getName());
    }

    @Test
    @Transactional
    public void getAllElements() throws Exception {
        Department department1 = EntityFactory.createEmptyDepartment();
        Department department2 = EntityFactory.createEmptyDepartment();
        Department department3 = EntityFactory.createEmptyDepartment();
        List<Department> departments = new ArrayList<>();
        departments.add(department1);
        departments.add(department2);
        departments.add(department3);
        departmentDao.addAll(departments);
        List<Department> receivedDepartments = (List<Department>) departmentDao.getAllElements();
        Assert.assertEquals(departments.size() + tableCount, receivedDepartments.size());
    }


    @Test
    @Transactional
    public void deleteElement() throws Exception {

        List<Department> departments = (List<Department>) departmentDao.getAllElements();
        departments.stream().forEach(department -> System.out.println(department.getId()));

        Department test = EntityFactory.createEmptyDepartment();
        test.setName("forDelete");
        Long idTest = departmentDao.addElement(test).getId();
        departmentDao.deleteElement(idTest);
        Assert.assertNull(departmentDao.getElementByID(idTest));
    }

    @Test
    @Transactional
    public void getFew() throws Exception {
        Department department1 = EntityFactory.createEmptyDepartment();
        department1.setName("Few Department 1");
        Department department2 = EntityFactory.createEmptyDepartment();
        department2.setName("Few Department 2");
        List<Department> departments = new ArrayList<>();
        departments.add(department1);
        departments.add(department2);
        departmentDao.addAll(departments);
        List<Department> receivedDepartments = (List<Department>) departmentDao.getFew(2, 3);
        Assert.assertEquals(3, receivedDepartments.size());
    }

    @Test
    @Transactional
    public void getRecordsCount() {
        long count = departmentDao.getRecordsCount();
        assertEquals(3L, count); // 1L - number of records satisfying query conditions added in setUp
    }

    @Test
    @Transactional
    public void getDepartmentCommonItemsCount() {
        long count = departmentDao.getDepartmentCommonItemsCount(addedDepartmentIDs.get(0));
        assertEquals(1L, count); // 1L - number of records satisfying query conditions added in setUp
    }

    @Test
    @Transactional
    public void getDepartmentUsersCount() {
        long count = departmentDao.getDepartmentUsersCount(addedDepartmentIDs.get(0));
        assertEquals(2L, count); // 1L - number of records satisfying query conditions added in setUp
    }

    @After
    public void tearDown() throws Exception {
        for (Long id : addedDepartmentIDs) {
            departmentDao.deleteElement(id);
        }
    }
}
