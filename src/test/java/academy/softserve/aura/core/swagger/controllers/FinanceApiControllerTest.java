package academy.softserve.aura.core.swagger.controllers;

import academy.softserve.aura.core.entity.ItemAmortization;
import academy.softserve.aura.core.entity.ItemAmortizationStatistic;
import academy.softserve.aura.core.mappers.DtoMapper;
import academy.softserve.aura.core.services.FinancialService;
import academy.softserve.aura.core.swagger.model.ItemAmortizationDto;
import academy.softserve.aura.core.swagger.model.ItemAmortizationStatisticDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FinanceApiControllerTest {

    @Mock
    private FinancialService financialService;

    @Mock
    private DtoMapper<ItemAmortizationDto, ItemAmortization> itemAmortizationMapper;

    @Mock
    private DtoMapper<ItemAmortizationStatisticDto, ItemAmortizationStatistic> itemAmortizationStatisticMapper;

    @InjectMocks
    private FinanceApiController financeApiController;

    List<ItemAmortizationDto> reportList = new ArrayList<>();
    List<ItemAmortizationStatisticDto> statisticList = new ArrayList<>();

    List<ItemAmortization> reportEntityList = new ArrayList<>();
    List<ItemAmortizationStatistic> statisticEntityList = new ArrayList<>();

    ItemAmortizationDto report = new ItemAmortizationDto();
    ItemAmortization reportEntity = new ItemAmortization();

    Integer offset = 0;
    Integer limit = 20;
    Long id = Long.valueOf(1002);

    @Before
    public void setUp(){

        when(financialService.getAmortizationReport(anyInt(), anyInt())).thenReturn(reportEntityList);
        when(financialService.getAmortizationById(anyLong())).thenReturn(reportEntity);
        when(financialService.getAmortizationStatisticByItemId(id, offset, limit)).thenReturn(statisticEntityList);

        when(itemAmortizationMapper.toDto(reportEntity)).thenReturn(report);
        when(itemAmortizationMapper.toEntity(report)).thenReturn(reportEntity);
    }

    @Test
    public void invokeGetAmortizationReportTest(){
        assertEquals(new ResponseEntity<>(reportList, HttpStatus.OK),
                financeApiController.getAmortizationReport(offset, limit));
        Mockito.verify(financialService, Mockito.times(1)).getAmortizationReport(anyInt(), anyInt());
    }

    @Test
    public void invokeGetAmortizationReportByItemIdTest(){
        assertEquals(new ResponseEntity<>(report, HttpStatus.OK),
                financeApiController.getAmortizationReportByItemId(id));
        Mockito.verify(financialService, Mockito.times(1)).getAmortizationById(anyLong());
    }

    @Test
    public void invokeGetAmortizationStatisticByItemId(){
        assertEquals(new ResponseEntity<>(statisticList, HttpStatus.OK),
                financeApiController.getAmortizationStatisticByItemId(id, offset, limit));
        Mockito.verify(financialService, Mockito.times(1)).getAmortizationStatisticByItemId(anyLong(), anyInt(), anyInt());
    }
}