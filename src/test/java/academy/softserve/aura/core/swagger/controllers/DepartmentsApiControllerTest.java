package academy.softserve.aura.core.swagger.controllers;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.mappers.ContactsMapper;
import academy.softserve.aura.core.mappers.DepartmentMapper;
import academy.softserve.aura.core.mappers.DtoMapper;
import academy.softserve.aura.core.services.DepartmentService;
import academy.softserve.aura.core.swagger.model.CommonItemDto;
import academy.softserve.aura.core.swagger.model.DepartmentDto;
import academy.softserve.aura.core.swagger.model.UserDto;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static academy.softserve.aura.core.utils.EntityFactory.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentsApiControllerTest {

    @InjectMocks
    private DepartmentsApiController departmentsApiController;

    @Mock
    private DtoMapper<DepartmentDto, Department> departmentDtoMapper = new DepartmentMapper(new ContactsMapper());
    @Mock
    private DtoMapper<CommonItemDto, CommonItem> commonItemDtoMapper;
    @Mock
    private DtoMapper<UserDto, User> userDtoMapper;
    @Mock
    private DepartmentService departmentService;

    private Department entity = createEmptyDepartment();
    private DepartmentDto dto = departmentDtoMapper.toDto(entity);
    private Long departmentId = entity.getId();
    private HttpHeaders headers = new HttpHeaders();
    private int offset = 0;
    private int limit = 10;

    @Before
    public void setUp() {
        headers.add("X-total-records-count", "0");
    }

    @Test
    public void addExistingDepartment() {
        when(departmentService.create(entity)).thenReturn(entity);
        when(departmentDtoMapper.toEntity(dto)).thenReturn(entity);
        when(departmentDtoMapper.toDto(entity)).thenReturn(dto);

        assertEquals(new ResponseEntity<>(dto, HttpStatus.OK),
                departmentsApiController.addDepartment(dto));
        Mockito.verify(departmentService, Mockito.times(1)).create(entity);
    }

    @Test
    public void addNotExistingDepartment() {
        DepartmentDto departmentDto = Mockito.mock(DepartmentDto.class);

        when(departmentService.create(entity)).thenReturn(entity);
        when(departmentDtoMapper.toDto(entity)).thenReturn(departmentDto);
        when(departmentDtoMapper.toEntity(departmentDto)).thenReturn(entity);
        when(departmentDto.getId()).thenReturn(null);

        assertEquals(new ResponseEntity<>(departmentDto, HttpStatus.CREATED),
                departmentsApiController.addDepartment(departmentDto));
        Mockito.verify(departmentService, Mockito.times(1)).create(entity);
    }

    @Test
    public void deleteExistingDepartment() {
        when(departmentService.delete(departmentId)).thenReturn(true);

        assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT),
                departmentsApiController.deleteDepartment(departmentId));

        Mockito.verify(departmentService, Mockito.times(1)).delete(departmentId);
    }

    @Test
    public void deleteNotExistingDepartment() {
        when(departmentService.delete(departmentId)).thenReturn(false);

        assertEquals(new ResponseEntity<>(HttpStatus.NOT_FOUND),
                departmentsApiController.deleteDepartment(departmentId));

        Mockito.verify(departmentService, Mockito.times(1)).delete(departmentId);
    }

    @Test
    public void getDepartments() {
        List<Department> departmentList = createEmptyDepartmentList(limit);
        List<DepartmentDto> departmentDtoList = departmentList.stream()
                .map(d -> departmentDtoMapper.toDto(d)).collect(Collectors.toList());

        when(departmentService.findFew(offset, limit)).thenReturn(departmentList);

        assertEquals(new ResponseEntity<>(departmentDtoList, headers, HttpStatus.OK),
                departmentsApiController.getDepartments(offset, limit));
        Mockito.verify(departmentService, Mockito.times(1)).findFew(offset, limit);
    }

    @Test
    public void getExistingDepartmentById() {
        when(departmentService.findById(departmentId)).thenReturn(null);
        assertEquals(new ResponseEntity<>(HttpStatus.NOT_FOUND),
                departmentsApiController.getDepartmentById(departmentId));
        Mockito.verify(departmentService, Mockito.times(1)).findById(departmentId);
    }

    @Test
    public void getNotExistingDepartmentById() {
        when(departmentService.findById(departmentId)).thenReturn(entity);
        when(departmentDtoMapper.toDto(entity)).thenReturn(dto);

        assertEquals(new ResponseEntity<>(dto, HttpStatus.OK),
                departmentsApiController.getDepartmentById(departmentId));
        Mockito.verify(departmentService, Mockito.times(1)).findById(departmentId);
    }

    @Test
    public void getExistingCommonItemsFromDepartment() {
        List<CommonItem> itemList = new ArrayList<>();
        List<CommonItemDto> itemDtoList = new ArrayList<>();
        CommonItem item = createComplexCommonItem();

        itemList.add(item);
        itemDtoList.add(commonItemDtoMapper.toDto(createComplexCommonItem()));

        when(departmentService.getDepartmentCommonItems(departmentId)).thenReturn(itemList);

        assertEquals(new ResponseEntity<>(itemDtoList, headers, HttpStatus.OK),
                departmentsApiController.getDepartmentCommonItems(departmentId, offset, limit));
        Mockito.verify(departmentService, Mockito.times(1))
                .getDepartmentCommonItems(departmentId);
    }

    @Test
    public void getNotExistingCommonItemsFromDepartment() {
        List<CommonItem> itemList = new ArrayList<>();
        List<CommonItemDto> itemDtoList = new ArrayList<>();

        when(departmentService.getDepartmentCommonItems(departmentId)).thenReturn(itemList);

        assertEquals(new ResponseEntity<>(headers, HttpStatus.NOT_FOUND),
                departmentsApiController.getDepartmentCommonItems(departmentId, offset, limit));
        Mockito.verify(departmentService, Mockito.times(1))
                .getDepartmentCommonItems(departmentId);
    }

    @Test
    public void getExistingUsersFromDepartment() {
        List<User> userList = new ArrayList<>();
        List<UserDto> userDtoList = new ArrayList<>();
        User user = createUserWithCredentialsAndNewDepartment();

        userList.add(user);
        userDtoList.add(userDtoMapper.toDto(user));

        when(departmentService.getDepartmentUsers(departmentId)).thenReturn(userList);

        assertEquals(new ResponseEntity<>(userDtoList, headers, HttpStatus.OK),
                departmentsApiController.getDepartmentUsers(departmentId, offset, limit));
        Mockito.verify(departmentService, Mockito.times(1))
                .getDepartmentUsers(departmentId);
    }

    @Test
    public void getNotExistingUsersFromDepartment() {
        List<User> userList = new ArrayList<>();

        when(departmentService.getDepartmentUsers(departmentId)).thenReturn(userList);

        assertEquals(new ResponseEntity<>(headers, HttpStatus.NOT_FOUND),
                departmentsApiController.getDepartmentUsers(departmentId, offset, limit));
        Mockito.verify(departmentService, Mockito.times(1))
                .getDepartmentUsers(departmentId);
    }

    @Test
    public void updateExistingDepartmentById() {
        when(departmentDtoMapper.toEntity(dto)).thenReturn(entity);
        when(departmentDtoMapper.toDto(entity)).thenReturn(dto);
        when(departmentService.update(entity)).thenReturn(entity);

        assertEquals(new ResponseEntity<>(dto, HttpStatus.OK),
                departmentsApiController.updateDepartmentById(departmentId, dto));
        Mockito.verify(departmentService, Mockito.times(1))
                .update(entity);
    }

    @Test
    public void updateNotExistingDepartmentById() {
        Department toReturn = EntityFactory.createEmptyDepartment();

        toReturn.setId(entity.getId() + 1);

        when(departmentDtoMapper.toEntity(dto)).thenReturn(entity);
        when(departmentDtoMapper.toDto(toReturn)).thenReturn(dto);
        when(departmentService.update(entity)).thenReturn(toReturn);

        assertEquals(new ResponseEntity<>(dto, HttpStatus.CREATED),
                departmentsApiController.updateDepartmentById(departmentId, dto));
        Mockito.verify(departmentService, Mockito.times(1))
                .update(entity);
    }

}
