package academy.softserve.aura.core.swagger.integration;

import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.security.jwt.UserCredentialsInfo;
import academy.softserve.aura.core.swagger.config.XmlConverterConfig;
import academy.softserve.aura.core.swagger.dto.DtoEntitiesFactory;
import academy.softserve.aura.core.swagger.model.DepartmentDto;
import academy.softserve.aura.core.swagger.model.UserDto;
import academy.softserve.aura.core.swagger.model.UserWithCredentialsDto;
import academy.softserve.aura.core.utils.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.swagger.configuration.RFC3339DateFormat;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CrudTest {


    private static final String BASE_API_URL = "http://localhost:12000/api";

    private static List<Long> departmentIds = new ArrayList<>();
    private static List<Long> userIds = new ArrayList<>();


    private static RestTemplate restTemplate;

    private static Server server;

    private static DepartmentDto addedDepartmentDto;
    private static UserDto addedUserDto;


    public static String token = null;

    @BeforeClass
    public static void configure() throws Exception {
        CloseableHttpClient httpClient = HttpClientBuilder
                .create()
                .setRedirectStrategy(LaxRedirectStrategy.INSTANCE)
                .build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);

        restTemplate = new WithTokenRestTemplate(factory);

        restTemplate.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return (response.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR);
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {

            }
        });

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
//        messageConverters.add(new MappingJackson2HttpMessageConverter());
        ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.xml()
                .annotationIntrospector(new XmlConverterConfig().new CustomAnnotationIntrospector())
                .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .dateFormat(new RFC3339DateFormat())
                .build();
        MappingJackson2XmlHttpMessageConverter converter = new MappingJackson2XmlHttpMessageConverter(objectMapper);
        messageConverters.add(converter);
        messageConverters.add(new FormHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);

        System.setProperty("java.naming.factory.url.pkgs", "org.eclipse.jetty.jndi");
        System.setProperty("java.naming.factory.initial", "org.eclipse.jetty.jndi.InitialContextFactory");

        server = new Server(12000);

        WebAppContext context = new WebAppContext();
        context.setDescriptor("src/test/resources/integration/web.xml");
        context.setResourceBase("src/main/webapp");
        context.setContextPath("/");
        context.setParentLoaderPriority(true);

        server.setHandler(context);

        server.start();

//        getAllDepartmentByIdUnauthorizedTest();

        login("superadmin", "mysecretpassword");
    }

    private static void login(String login, String password) {
        if (login.equals("superadmin") && password.equals("mysecretpassword")) {
            token = TokenUtils.generateToken(login, new UserCredentialsInfo(login, UserRole.ADMIN));
        }
    }

    private void logout() {
        token = null;
    }

//      cant use it in case user is redirected to html for login
//    public static void getAllDepartmentByIdUnauthorizedTest() {
//        ResponseEntity<Void> response = restTemplate.getForEntity(BASE_API_URL + "/departments/" + 5, Void.class);
//        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
//    }

    @Test
    public void _001createDepartmentTest() {

        DepartmentDto departmentDto = DtoEntitiesFactory.createEmptyDepartment();
        ResponseEntity<DepartmentDto> response =
                restTemplate.postForEntity(BASE_API_URL + "/departments", departmentDto, DepartmentDto.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        DepartmentDto added = response.getBody();
        assertNotNull(added.getId());
        addedDepartmentDto = added;
        System.out.println(added);

        departmentIds.add(addedDepartmentDto.getId());
    }

    @Test
    public void _002createUserTest() {
        UserWithCredentialsDto userWithCredentialsDto = DtoEntitiesFactory.createUserWithCredentialsDto();
        ResponseEntity<UserDto> response = restTemplate.postForEntity(BASE_API_URL + "/users", userWithCredentialsDto, UserDto.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        UserDto added = response.getBody();
        assertNotNull(added.getId());
        addedUserDto = added;
        System.out.println(added);

        userIds.add(addedUserDto.getId());
    }

    @Test
    @Ignore
//    in memory db does not create not null constraint, but in real db it works fine
    public void _002aCreateUserWithLoginDuplicationGetsBadRequest() {
        UserWithCredentialsDto userWithCredentialsDto = DtoEntitiesFactory.createUserWithCredentialsDto();
        ResponseEntity<UserDto> responseOne = restTemplate.postForEntity(BASE_API_URL + "/users", userWithCredentialsDto, UserDto.class);
        UserDto added = responseOne.getBody();

        assertEquals(HttpStatus.CREATED, responseOne.getStatusCode());
        assertNotNull(added.getId());

        ResponseEntity<UserDto> responseTwo = restTemplate.postForEntity(BASE_API_URL + "/users", userWithCredentialsDto, UserDto.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseTwo.getStatusCode());

    }


    @Test
    public void _003getDepartmentByIdTest() {
        Long idOfLastAdded = addedDepartmentDto.getId();
        assertNotNull(idOfLastAdded);
        ResponseEntity<DepartmentDto> response = restTemplate.getForEntity(BASE_API_URL + "/departments/" + idOfLastAdded, DepartmentDto.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        DepartmentDto gotDepartment = response.getBody();
        assertEquals(addedDepartmentDto, gotDepartment);
        System.out.println(gotDepartment);
    }

    @Test
    public void _004getDepartmentByWrongId() {
        Long wrongId = -23L;
        ResponseEntity responseEntity = restTemplate.getForEntity(BASE_API_URL + "/departments/" + wrongId, DepartmentDto.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    public void _005getDepartmentByNonExistingId() {
        Long id = System.currentTimeMillis();
        ResponseEntity responseEntity = restTemplate.getForEntity(BASE_API_URL + "/departments/" + id, DepartmentDto.class);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void _006getUserByIdTest() {
        Long idOfLastAdded = addedUserDto.getId();
        assertNotNull(idOfLastAdded);
        ResponseEntity<UserDto> response = restTemplate.getForEntity(BASE_API_URL + "/users/" + idOfLastAdded, UserDto.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        UserDto gotUser = response.getBody();
        assertEquals(addedUserDto, gotUser);
        System.out.println(gotUser);
    }

    @Test
    public void _007getUserByWrongId() {
        Long wrongId = -23L;
        ResponseEntity responseEntity = restTemplate.getForEntity(BASE_API_URL + "/users/" + wrongId, UserDto.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    public void _008getUserByNonExistingId() {
        Long id = System.currentTimeMillis();
        ResponseEntity responseEntity = restTemplate.getForEntity(BASE_API_URL + "/users/" + id, UserDto.class);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void _009updateExistingDepartment() {
        addedDepartmentDto.setName("modified for update");

        ResponseEntity<DepartmentDto> responseEntity = restTemplate.exchange(
                BASE_API_URL + "/departments/" + addedDepartmentDto.getId(),
                HttpMethod.PUT,
                new HttpEntity<>(addedDepartmentDto),
                DepartmentDto.class
        );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        assertEquals(addedDepartmentDto, responseEntity.getBody());
    }

    @Test
    public void _010updateNonExistingDepartment() {
        addedDepartmentDto.setName("non existing one, should be added");
        addedDepartmentDto.setId(System.currentTimeMillis());

        ResponseEntity<DepartmentDto> responseEntity = restTemplate.exchange(
                BASE_API_URL + "/departments/" + addedDepartmentDto.getId(),
                HttpMethod.PUT,
                new HttpEntity<>(addedDepartmentDto),
                DepartmentDto.class
        );

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());

        DepartmentDto added = responseEntity.getBody();

        assertNotNull(added.getId());

        addedDepartmentDto.setId(added.getId());

        assertEquals(addedDepartmentDto, added);

    }

    @Test
    public void _011removeNonExistingDepartment() {
        long id = System.currentTimeMillis();

        ResponseEntity<Void> removeResponse = restTemplate.exchange(
                BASE_API_URL + "/departments/" + id,
                HttpMethod.DELETE,
                HttpEntity.EMPTY,
                Void.class);

        assertEquals(HttpStatus.NOT_FOUND, removeResponse.getStatusCode());
    }

    @Test
    public void _012removeExistingDepartment() {

        _001createDepartmentTest();

//        restTemplate.delete(BASE_API_URL + "/departments/" + addedDepartmentDto.getId());
        ResponseEntity<Void> removeResponse = sendOnDelete(BASE_API_URL + "/departments/" + addedDepartmentDto.getId());

        assertEquals(HttpStatus.NO_CONTENT, removeResponse.getStatusCode());

        ResponseEntity<DepartmentDto> response = restTemplate.getForEntity(BASE_API_URL + "/departments/" + addedDepartmentDto.getId(), DepartmentDto.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    private static ResponseEntity<Void> sendOnDelete(String url) {
        return restTemplate.exchange(url,
                HttpMethod.DELETE,
                HttpEntity.EMPTY,
                Void.class);
    }

    @Test
    public void _013setDepartmentAndUpdateExistingUser() {
//        add department as far as it was removed
        _001createDepartmentTest();

        addedUserDto.setDepartmentId(addedDepartmentDto.getId());
        sendOnPut(addedUserDto);

        ResponseEntity response = sendOnPut(addedUserDto);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        assertEquals(addedUserDto, response.getBody());

    }

    @Test
    public void _014updateNonExistingUser() {
        Long id = System.currentTimeMillis();
        addedUserDto.setId(id);
        addedUserDto.setFirstName("Non existing user, should be persisted");

        ResponseEntity<UserDto> response = restTemplate.exchange(
                BASE_API_URL + "/users/" + id,
                HttpMethod.PUT,
                new HttpEntity<>(addedUserDto),
                UserDto.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());


    }

    @Test
    public void _015removeNonExistingUser() {
        long id = System.currentTimeMillis();

        ResponseEntity<Void> removeResponse = sendOnDelete(BASE_API_URL + "/users/" + id);

        assertEquals(HttpStatus.NOT_FOUND, removeResponse.getStatusCode());
    }

    @Test
    public void _016removeExistingUser() {
        _002createUserTest();

        ResponseEntity<Void> removeResponse = sendOnDelete(BASE_API_URL + "/users/" + addedUserDto.getId());

        assertEquals(HttpStatus.NO_CONTENT, removeResponse.getStatusCode());

        ResponseEntity<UserDto> response = restTemplate.getForEntity(BASE_API_URL + "/users/" + addedUserDto.getId(), UserDto.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        assertEquals(false, response.getBody().getIsActive());
    }

    @Test
    public void _017getAllDepartmentsWithWrongLimitOffset() {
        ResponseEntity response = restTemplate.getForEntity(
                BASE_API_URL + "/departments?limit=-12&offset=-5",
                Object.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void _018getAllDepartmentsWithLimitOffset() {
        ResponseEntity<DepartmentDto[]> response = restTemplate.getForEntity(
                BASE_API_URL + "/departments?limit=5&offset=0",
                DepartmentDto[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        DepartmentDto[] body = response.getBody();
        assertNotNull(body);
        Arrays.stream(body).forEach(System.out::println);
    }

    @Test
    public void _019getAllUsersWithWrongLimitOffset() {
        ResponseEntity response = restTemplate.getForEntity(
                BASE_API_URL + "/users?limit=-12&offset=-5",
                Object.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void _020getAllUsersWithLimitOffset() {
        _001createDepartmentTest();
        _002createUserTest();
        _013setDepartmentAndUpdateExistingUser();

        ResponseEntity<UserDto[]> response = restTemplate.getForEntity(
                BASE_API_URL + "/users?limit=5&offset=0&fromDept=" + addedDepartmentDto.getId() + "&fromRole=" + UserRole.USER.name() + "&isActive=true",
                UserDto[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        UserDto[] body = response.getBody();
        assertNotNull(body);
        Arrays.stream(body).forEach(System.out::println);
        assertEquals(1, body.length);
    }

    @Test
    public void _021adminDeactivatesUserTest() {
        _002createUserTest();

        UserDto created = addedUserDto;
        created.setIsActive(false);

        ResponseEntity responseEntity = sendOnPut(created);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        System.out.println(responseEntity);
    }


    private ResponseEntity sendOnPut(UserDto created) {
        return restTemplate.exchange(
                BASE_API_URL + "/users/" + addedUserDto.getId(),
                HttpMethod.PUT,
                new HttpEntity<>(created),
                UserDto.class);
    }

    @AfterClass
    public static void removeAdded() {
        departmentIds.forEach(d -> sendOnDelete(BASE_API_URL + "/departments/" + d));
        userIds.forEach(u -> sendOnDelete(BASE_API_URL + "/users/" + u));
    }


    /**
     * Ovveriding Rest Template as far as we need put cookie ourself
     * it is done to prevent depency AuraCore tests from SSO server
     */
    private static class WithTokenRestTemplate extends RestTemplate {

        public WithTokenRestTemplate(HttpComponentsClientHttpRequestFactory factory) {
            super(factory);
        }

        /**
         * put cookie with token in request, if they exists
         */
        @Override
        protected ClientHttpRequest createRequest(URI url, HttpMethod method) throws IOException {
            ClientHttpRequest request = super.createRequest(url, method);
            if (token != null) request.getHeaders().add("Cookie", Constants.JWT_TOKEN_COOKIE_NAME + "=" + token);
            return request;
        }
    }


}
